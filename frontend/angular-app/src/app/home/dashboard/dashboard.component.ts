import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  //列表變數
  searchList: Array<any> = [];
  currentPage: number = 1;
  pageSize: number = 10;
  itemTotal: number = 0;
  //詳細打卡資料變數
  detailData: any = {
    pushDate: null,
    lists: []
  };
  loaderShow: boolean = false;

  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.refreshSearchList();
  }

  punch(): void {
    this.httpService.post('/api/Punch/PunchAsync', null).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: '打卡成功',
        showConfirmButton: false,
        scrollbarPadding: false,
        allowOutsideClick: false,
        timer: 1500,
      }).then(() => {
        this.refreshSearchList();
      });
    });
  }

  showDetail(item: any): void {
    this.detailData = {
      pushDate: item.pushDate,
      lists: item.pushTimes
    };
  }

  onChangePage(changeValue: any): void {
    this.currentPage = changeValue;
    this.refreshSearchList();
  }

  tablePageSize(): void {
    this.refreshSearchList();
  }

  refreshSearchList(): void {
    this.loaderShow = true;
    let jsonData = {
      Rows: this.currentPage,
      Page: this.pageSize
    };
    this.httpService.post('/api/Punch/GetPunchListAsync', jsonData, false).subscribe(res => {
      this.searchList = res.result;
      this.itemTotal = res.total;
      this.loaderShow = false;
    });
  }

}
