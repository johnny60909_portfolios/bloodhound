import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { AuthGuard } from '../shared/guard/auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ErrorComponent } from '../shared/component/error/error.component';
import { PunchModule } from '../punch/punch.module';
const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: DashboardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'account',
        loadChildren: () => import('../account/account.module').then((m) => m.AccountModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'punch',
        loadChildren: () => import('../punch/punch.module').then((m) => m.PunchModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'error',
        component: ErrorComponent,
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
