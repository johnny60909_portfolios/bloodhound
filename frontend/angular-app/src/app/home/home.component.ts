import { Component, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Inject, ElementRef } from '@angular/core';
import { Router, Event, NavigationStart } from '@angular/router';
import { SharedService } from '../shared/services/shared.service';
import { HttpService } from 'src/app/shared/services/http.service';
import { Observable } from 'rxjs';
import { emitDistinctChangesOnlyDefaultValue } from '@angular/compiler';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  today?: Date;
  menuObservable: any;
  constructor(
    @Inject(DOCUMENT)
    private document: Document,
    private elementRef: ElementRef,
    public sharedService: SharedService,
    private httpService: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.today = new Date();
    setInterval(() => {
      this.today = new Date();
    }, 1000);

    if (!this.sharedService.user.account) {
      this.sharedService.getUserData().subscribe(res => {
        this.sharedService.user.account = res.staffID;
        this.sharedService.user.roleName = res.roleName;
        this.sharedService.user.jobTitle = res.jobTitle;
        this.sharedService.user.gender = res.gender;
      });
    }

    this.getMenu();
  }

  ngAfterViewInit() {
    //Custom scripts for all pages
    this.menuObservable.subscribe((res: any) => {
      this.sharedService.menuList = res;

      var html = '';
      this.sharedService.menuList.forEach((group: any) => {
        var subHtml = '';
        group.subMenus.forEach((sub: any) => {
          subHtml += `
          <a class="collapse-item" href="javascript:void(0)" data-id="${group.menuGroupName}_${sub.menuName}">${sub.menuName}</a>
          `;
        });
        html += `
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapse_${group.menuGroupName}"
                aria-expanded="true" aria-controls="collapse_${group.menuGroupName}">
                <i class="${group.iconClass}"></i>
                <span>${group.menuGroupName}</span>
            </a>
            <div id="collapse_${group.menuGroupName}" class="collapse" aria-labelledby="headingPunch" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                  ${subHtml}
                </div>
            </div>
        </li> 
        `;
      });

      $('#menuTop').after(html);

      $('[class=collapse-item]').click(function (event: any) {
        var linkId = $(event.currentTarget).data('id');
        const customEvent = new Event('click');
        $(`#${linkId}`)[0].dispatchEvent(customEvent);
      });

      setTimeout(() => {
        var s = this.document.createElement("script");
        s.type = "text/javascript";
        s.src = "assets/js/sb-admin-2.min.js";
        this.elementRef.nativeElement.appendChild(s);
      }, 100);

      //檢查路由是否合法
      this.sharedService.checkPageRollback();

      //註冊監聽路由
      this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationStart) {
          this.sharedService.checkPageRollback(event.url);
        }
      });
    });
  }

  logout(): void {
    this.sharedService.removeTokens();
    this.sharedService.goBackToLogin();
  }

  getMenu(): Observable<any> {
    return this.menuObservable = this.httpService.get('/api/Menu/GetMenuAsync');
  }

  goLink(controller: any, action: any): void {
    this.router.navigate([`/${controller}/${action}`]);
  }

  getDashboardClass(): string {
    return this.router.url == '/' ? 'nav-item active' : 'nav-item';
  }

  getUserIcon(): string {
    var imgSrc = 'assets/img/undraw_profile_2.svg';
    if (this.sharedService.user.gender == 'F')
      imgSrc = 'assets/img/undraw_profile_3.svg';
    return imgSrc;
  }
}
