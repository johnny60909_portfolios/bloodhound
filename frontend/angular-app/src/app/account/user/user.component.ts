import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  permission: any = {};
  //列表變數
  searchList: Array<any> = [];
  currentPage: number = 1;
  pageSize: number = 10;
  itemTotal: number = 0;
  //詳細打卡資料變數
  detailData: any = {};
  roleKeyValue: Array<any> = [];
  loaderShow: boolean = false;

  constructor(
    public sharedService: SharedService,
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.refreshSearchList();
    this.httpService.get('/api/Role/GetAllRoleKeyValueAsync', false).subscribe(res => {
      this.roleKeyValue = res;
    });
  }

  tablePageSize(): void {
    this.refreshSearchList();
  }

  show_password(item: any): void {
    this.detailData.id = item.id;
    $("#passwordModal").modal('show');
  }

  save_password(): void {
    this.httpService.post('/api/User/ChangePassword', this.detailData).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: '修改成功',
        showConfirmButton: false,
        scrollbarPadding: false,
        allowOutsideClick: false,
        timer: 1500,
      }).then(() => {
        $("#passwordModal").modal('hide');
        this.refreshSearchList();
      });
    });
  }

  changeEnable(item: any, value: any): void {
    let jsonData = {
      Id: item.id,
      IsEnabled: value
    };
    this.httpService.post('/api/User/ChangeEnable', jsonData).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: '修改成功',
        showConfirmButton: false,
        scrollbarPadding: false,
        allowOutsideClick: false,
        timer: 1500,
      }).then(() => {
        this.refreshSearchList();
      });
    });
  }

  onChangePage(changeValue: any): void {
    this.currentPage = changeValue;
    this.refreshSearchList();
  }

  refreshSearchList(): void {
    this.loaderShow = true;
    let jsonData = {
      Rows: this.currentPage,
      Page: this.pageSize
    };
    this.httpService.post('/api/User/GetUserListAsync', jsonData, false).subscribe(res => {
      this.searchList = res.result;
      this.itemTotal = res.total;
      this.loaderShow = false;
    });
  }

}
