import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/services/shared.service';
import { HttpService } from 'src/app/shared/services/http.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-role-limit',
  templateUrl: './role-limit.component.html',
  styleUrls: ['./role-limit.component.css']
})
export class RoleLimitComponent implements OnInit {
  roleId: any;
  //詳細資料變數
  detailData: any = {};
  roleKeyValue: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    public sharedService: SharedService,
    private httpService: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.roleId = this.route.snapshot.paramMap.get('roleId');
    if (!this.roleId)
      this.sharedService.goBackToError();
    this.httpService.get(`/api/Role/GetRoleAsync?id=${this.roleId}`, false).subscribe({
      next: res => {
        this.detailData = res;
      },
      error: error => {
        this.sharedService.goBackToError();
      }
    });

    this.httpService.get('/api/Role/GetAllRoleKeyValueAsync', false).subscribe(res => {
      this.roleKeyValue = res;
    });
  }

  selectedAll(): void {
    this.detailData.roleFunctions.forEach((item: any) => {
      item.subFunctions.forEach((subItem: any) => {
        subItem.enabledOperations.forEach((operation: any) => {
          operation.operationIsEnable = true;
        });
      });
    });
  }

  cancelAll(): void {
    this.detailData.roleFunctions.forEach((item: any) => {
      item.subFunctions.forEach((subItem: any) => {
        subItem.enabledOperations.forEach((operation: any) => {
          operation.operationIsEnable = false;
        });
      });
    });
  }

  save(): void {
    this.httpService.post('/api/Role/EditRoleAsync', this.detailData).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: '新增成功',
        showConfirmButton: false,
        scrollbarPadding: false,
        allowOutsideClick: false,
        timer: 1500,
      }).then(() => {
        this.router.navigate(['/account/role']);
      });
    });
  }

}
