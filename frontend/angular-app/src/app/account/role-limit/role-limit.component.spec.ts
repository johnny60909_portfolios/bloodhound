import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleLimitComponent } from './role-limit.component';

describe('RoleLimitComponent', () => {
  let component: RoleLimitComponent;
  let fixture: ComponentFixture<RoleLimitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoleLimitComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RoleLimitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
