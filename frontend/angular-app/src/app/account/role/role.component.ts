import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  //列表變數
  searchList: Array<any> = [];
  currentPage: number = 1;
  pageSize: number = 10;
  itemTotal: number = 0;
  loaderShow: boolean = false;
  //詳細資料變數
  detailData: any = {};
  roleKeyValue: Array<any> = [];

  constructor(
    public sharedService: SharedService,
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.refreshSearchList();
    this.httpService.get('/api/Role/GetAllRoleKeyValueAsync', false).subscribe(res => {
      this.roleKeyValue = res;
    });
  }

  tablePageSize(): void {
    this.refreshSearchList();
  }

  onChangePage(changeValue: any): void {
    this.currentPage = changeValue;
    this.refreshSearchList();
  }

  changeEnable(item: any, value: any): void {
    let jsonData = {
      Id: item.id,
      IsEnabled: value
    };
    this.httpService.post('/api/Role/ChangeEnable', jsonData).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: '修改成功',
        showConfirmButton: false,
        scrollbarPadding: false,
        allowOutsideClick: false,
        timer: 1500,
      }).then(() => {
        this.refreshSearchList();
      });
    });
  }

  show_add(): void {
    this.detailData = {};
    $("#addModal").modal('show');
  }

  save_add(): void {
    this.httpService.post('/api/Role/CreateRoleAsync', this.detailData).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: '新增成功',
        showConfirmButton: false,
        scrollbarPadding: false,
        allowOutsideClick: false,
        timer: 1500,
      }).then(() => {
        $("#addModal").modal('hide');
        this.detailData = {};
        this.refreshSearchList();
      });
    });
  }

  refreshSearchList(): void {
    this.loaderShow = true;
    let jsonData = {
      Rows: this.currentPage,
      Page: this.pageSize
    };
    this.httpService.post('/api/Role/GetRoleListAsync', jsonData, false).subscribe(res => {
      this.searchList = res.result;
      this.itemTotal = res.total;
      this.loaderShow = false;
    });
  }
}
