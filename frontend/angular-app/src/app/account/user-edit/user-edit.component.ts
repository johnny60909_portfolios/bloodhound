import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/shared/services/shared.service';
import { HttpService } from 'src/app/shared/services/http.service';
import Swal from 'sweetalert2';
import * as moment from "moment";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  userId: any;
  //詳細資料變數
  detailData: any = {};
  roleKeyValue: Array<any> = [];
  //是否為編輯
  isEdit: boolean = true;
  //日曆初始時間
  startDate: any;
  constructor(
    private route: ActivatedRoute,
    public sharedService: SharedService,
    private httpService: HttpService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.startDate = moment().format();
    this.userId = this.route.snapshot.paramMap.get('userId');
    if (!this.userId)
      this.sharedService.goBackToError();
    if (this.userId == "add") {
      this.isEdit = false;
    }
    else {
      this.httpService.get(`/api/User/GetUserAsync?id=${this.userId}`, false).subscribe({
        next: res => {
          this.detailData = res;
        },
        error: error => {
          this.sharedService.goBackToError();
        }
      });
    }
    this.httpService.get('/api/Role/GetAllRoleKeyValueAsync', false).subscribe(res => {
      this.roleKeyValue = res;
    });
  }

  valuechange(name: string, newValue: boolean): void {
    this.detailData[name] = newValue;
  }

  save(): void {
    if (this.isEdit) {
      this.httpService.put('/api/User/EditUserAsync', this.detailData).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: '編輯成功',
          showConfirmButton: false,
          scrollbarPadding: false,
          allowOutsideClick: false,
          timer: 1500,
        }).then(() => {
          this.router.navigate(['/account/user']);
        });
      });
    }
    else {
      this.httpService.post('/api/User/CreateUserAsync', this.detailData).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: '新增成功',
          showConfirmButton: false,
          scrollbarPadding: false,
          allowOutsideClick: false,
          timer: 1500,
        }).then(() => {
          this.router.navigate(['/account/user']);
        });
      });
    }

  }
}
