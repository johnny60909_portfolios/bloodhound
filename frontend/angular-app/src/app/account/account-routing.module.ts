import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RoleComponent } from './role/role.component';
import { RoleLimitComponent } from './role-limit/role-limit.component';
import { UserEditComponent } from './user-edit/user-edit.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'user',
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: UserComponent,
  },
  {
    path: 'user/:userId',
    component: UserEditComponent,
  },
  {
    path: 'role',
    component: RoleComponent,
  },
  {
    path: 'role/:roleId',
    component: RoleLimitComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
