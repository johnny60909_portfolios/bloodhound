import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import * as moment from "moment";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  //查詢變數
  searchFilter: any = {
    startDate: '',
    endDate: '',
    userId: ''
  };
  //列表變數
  searchList: Array<any> = [];
  currentPage: number = 1;
  pageSize: number = 10;
  itemTotal: number = 0;
  //詳細打卡資料變數
  detailData: any = {
    userName: '',
    pushDate: null,
    lists: []
  };
  userKeyValue: Array<any> = [];
  loaderShow: boolean = false;
  startDate: any;
  constructor(
    private httpService: HttpService
  ) { }

  ngOnInit(): void {
    this.startDate = moment().format();
    this.refreshSearchList();
    this.httpService.get('/api/User/GetAllUserKeyValueAsync', false).subscribe(res => {
      this.userKeyValue.push({ key: '', value: '請選擇' });
      res.forEach((item: any) => {
        this.userKeyValue.push(item);
      });
    });
  }

  clear(): void {
    this.searchFilter = {
      startDate: '',
      endDate: '',
      userId: ''
    };
  }

  search(): void {
    this.refreshSearchList();
  }

  showDetail(item: any): void {
    this.detailData = {
      userName: item.userName,
      pushDate: item.pushDate,
      lists: item.pushTimes
    };
  }

  onChangePage(changeValue: any): void {
    this.currentPage = changeValue;
    this.refreshSearchList();
  }

  tablePageSize(): void {
    this.refreshSearchList();
  }

  refreshSearchList(): void {
    this.loaderShow = true;
    var pageSetting = {
      Rows: this.currentPage,
      Page: this.pageSize
    };
    let jsonData = {
      StartDate: this.searchFilter.startDate,
      EndDate: this.searchFilter.endDate,
      UserId: this.searchFilter.userId,
      PageSetting: pageSetting
    };
    this.httpService.post('/api/aggregation/Punch/GetPunchListAsync', jsonData, false).subscribe(res => {
      this.searchList = res.result;
      this.itemTotal = res.total;
      this.loaderShow = false;
    });
  }

}
