/** 使用者 */
export class UserInfo {
  /** 帳號 */
  account?: string;
  /** 使用者名稱 */
  name?: string;
  /** 角色名稱 */
  roleName?: string;
  /** 職稱 */
  jobTitle?: string;
  /** 性別 */
  gender?: string;
}

