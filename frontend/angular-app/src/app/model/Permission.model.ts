/** 權限 */
export class Permission {
    /** 新增 */
    canInsert: boolean = false;
    /** 修改 */
    canUpdate: boolean = false;
    /** 刪除 */
    canDelete: boolean = false;
    /** 下載 */
    canDownload: boolean = false;
    /** 瀏覽 */
    canBrowse: boolean = false;
    /** 是否返回 */
    rollback: boolean = false;
}