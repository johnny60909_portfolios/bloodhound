import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Router } from '@angular/router';
import { UserInfo } from '../../model/UserInfo.model';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { Permission } from 'src/app/model/Permission.model';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private readonly Auth_Token = 'authtoken';
  private readonly Expires_Time = 'expirestime';
  user = new UserInfo();
  menuList: Array<any> = [];
  permission = new Permission();
  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  /** 回到首頁 */
  goBackToHome() {
    this.router.navigate(['/']);
  }

  /** 回到登入頁 */
  goBackToLogin() {
    this.router.navigate(['/auth/login']);
  }

  /** 回到錯誤頁 */
  goBackToError() {
    this.router.navigate(['/error']);
  }

  /** 取得User資料 */
  getUserData() {
    return this.httpService.get('/api/User/GetLoginUserAsync');
  }

  /** 取得Token */
  getAuthToken() {
    var token = localStorage.getItem(this.Auth_Token);
    if (token) {
      var expires = localStorage.getItem(this.Expires_Time);
      if (!expires)
        return null;
      var nowTime = moment().toDate();
      var expiresTime = moment(expires);
      if (expiresTime.isSameOrAfter(nowTime)) {
        return token;
      }
      else {
        this.removeTokens();
        return null;
      }
    }
    else
      return null;
  }

  /** 設定Token */
  storeTokens(token: string, expiresTime: string) {
    localStorage.setItem(this.Auth_Token, token);
    localStorage.setItem(this.Expires_Time, expiresTime);
  }

  /** 清除Token */
  removeTokens() {
    localStorage.setItem(this.Auth_Token, '');
    localStorage.setItem(this.Expires_Time, '');
  }

  /** 顯示驗證訊息 */
  showValidatorMsg(msg: string) {
    Swal.fire({
      icon: 'warning',
      title: msg,
      showConfirmButton: true,
      scrollbarPadding: false,
      allowOutsideClick: false,
    });
  }

  /** 檢查是否有權限進入頁面 */
  checkPageRollback(targetUrl: string = '') {
    this.permission = new Permission();
    var currentUrl = this.router.url;
    if (targetUrl)
      currentUrl = targetUrl;
    if (currentUrl == '/' || currentUrl.indexOf('/auth') != -1 || currentUrl.indexOf('/error') != -1)
      return;
    var sub = this.menuList.flatMap(a => a.subMenus).find((b: any) => currentUrl.indexOf(`${b.controller}/${b.action}`) != -1);
    if (!sub || sub.permission.rollback)
      this.router.navigate(['/error']);
    else {
      this.permission = sub.permission;
      if (currentUrl.indexOf('add') != -1 && !this.permission.canInsert) {
        this.router.navigate(['/error']);
      }
    }
  }
}
