import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public loaderShow = false;
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  /** HttpGET */
  get(api: string, hasLoading: boolean = true): Observable<any> {
    if (hasLoading)
      this.loaderShow = true;
    return this.http.get<any>(this.apiUrl + api);
  }

  /** HttpPUT */
  put(api: string, data: any, hasLoading: boolean = true): Observable<any> {
    if (hasLoading)
      this.loaderShow = true;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.put<any>(this.apiUrl + api, data, httpOptions);
  }

  /** HttpPOST */
  post(api: string, data: any, hasLoading: boolean = true): Observable<any> {
    if (hasLoading)
      this.loaderShow = true;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return this.http.post<any>(this.apiUrl + api, data, httpOptions);
  }

  /** HttpDELETE */
  delete(api: string, hasLoading: boolean = true): Observable<any> {
    if (hasLoading)
      this.loaderShow = true;
    return this.http.delete<any>(this.apiUrl + api);
  }
}

