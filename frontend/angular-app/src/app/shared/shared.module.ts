import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './component/loader/loader.component';
import { PaginationComponent } from './component/pagination/pagination.component';
import { TimePipe } from './pipe/time.pipe';
import { ErrorComponent } from './component/error/error.component';
import { TableLoaderComponent } from './component/table-loader/table-loader.component';
import {
  NgxMatDatetimePickerModule,
  NgxMatTimepickerModule,
} from "@angular-material-components/datetime-picker";
import { NgxMatMomentModule } from "@angular-material-components/moment-adapter";
import { MomentDateAdapter, MatMomentDateModule } from "@angular/material-moment-adapter";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_LOCALE, DateAdapter, } from '@angular/material/core';
import { RouterModule } from '@angular/router';

@NgModule({
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_LOCALE, useValue: "zh-TW" },
  ],
  declarations: [
    LoaderComponent,
    PaginationComponent,
    TimePipe,
    ErrorComponent,
    TableLoaderComponent
  ],
  imports: [
    CommonModule,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
    RouterModule
  ],
  exports: [
    FormsModule,
    HttpClientModule,
    LoaderComponent,
    TableLoaderComponent,
    PaginationComponent,
    TimePipe,
    MatDatepickerModule,
    MatMomentDateModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
  ]
})
export class SharedModule { }
