import { Component, Input, Output, OnInit, EventEmitter, } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Input() currentPage?: number;
  @Input() pageSize?: number;
  @Input() itemTotal?: number;
  @Output() changePage = new EventEmitter();

  //分頁最大顯示格子數量
  pageLength: any = 7;
  items: Array<any> = [];
  _currentPage: number;
  _pageSize: number;
  _itemTotal: number;

  constructor() {
    this.items = [];
    //當前頁數
    this._currentPage = this.currentPage ?? 1;
    // 一頁幾筆
    this._pageSize = this.pageSize ?? 10;
    //總數量
    this._itemTotal = this.itemTotal ?? 0;
  }

  ngOnInit(): void {
    this.refreshItems();
  }

  ngOnChanges(changes: any): void {
    if (changes.currentPage && changes.currentPage.currentValue) {
      this._currentPage = changes.currentPage.currentValue;
    }
    if (changes.pageSize && changes.pageSize.currentValue) {
      this._pageSize = changes.pageSize.currentValue;
    }
    if (changes.itemTotal && changes.itemTotal.currentValue) {
      this._itemTotal = changes.itemTotal.currentValue;
    }
    this.refreshItems();
  }

  refreshItems(): void {
    this.items = [];
    if (this._pageSize == 0 || this._itemTotal == 0)
      return;
    var lastPage = (this._itemTotal % this._pageSize) == 0 ? (this._itemTotal / this._pageSize) : Math.floor(this._itemTotal / this._pageSize) + 1;
    if (lastPage == 0)
      lastPage++;
    if (this._currentPage > lastPage) {
      this._currentPage = lastPage;
    }


    if (lastPage <= this.pageLength) {
      for (var i = 1; i <= lastPage; i++) {
        this.items.push(
          {
            type: 0,
            value: i.toString(),
            selected: i == this._currentPage
          });
        if (this.items.length >= this.pageLength) {
          break;
        }
      }
    }
    else {
      var rightValue = lastPage - (this.pageLength - 3);
      var leftValue = 1 + (this.pageLength - 3);
      //左邊從1開始
      if (this._currentPage < leftValue) {
        for (var i = 1; i <= leftValue; i++) {
          this.items.push(
            {
              type: 0,
              value: i.toString(),
              selected: i == this._currentPage
            });
        }
        this.items.push(
          {
            type: 1,
            value: "...",
            selected: false
          });
        this.items.push(
          {
            type: 0,
            value: lastPage.toString(),
            selected: false
          });
      }
      //右邊從底部往前
      else if (this._currentPage > rightValue) {
        this.items.push(
          {
            type: 0,
            value: "1",
            selected: false
          });
        this.items.push(
          {
            type: 0,
            value: "...",
            selected: false
          });
        for (var i = rightValue; i <= lastPage; i++) {
          this.items.push(
            {
              type: 0,
              value: i.toString(),
              selected: i == this._currentPage
            });
        }
      }
      //中間
      else {
        this.items.push(
          {
            type: 0,
            value: "1",
            selected: false
          });
        this.items.push(
          {
            type: 1,
            value: "...",
            selected: false
          });
        this.items.push(
          {
            type: 0,
            value: (this._currentPage - 1).toString(),
            selected: false
          });
        this.items.push(
          {
            type: 0,
            value: this._currentPage.toString(),
            selected: true
          });
        this.items.push(
          {
            type: 0,
            value: (this._currentPage + 1).toString(),
            selected: false
          });
        this.items.push(
          {
            type: 1,
            value: "...",
            selected: false
          });
        this.items.push(
          {
            type: 0,
            value: lastPage.toString(),
            selected: false
          });
      }
    }

    if (this.items.length == 0) {
      return;
    }

    var selectedIndex = this.items.findIndex((a: any) => a.selected);
    var maxShowValue = this.items[selectedIndex].value + this.pageLength - selectedIndex + 1;
    if (lastPage > maxShowValue) {
      this.items[this.items.length - 1] =
      {
        type: 0,
        value: lastPage.toString(),
        selected: lastPage == this._currentPage
      };
      this.items[this.items.length - 2] =
      {
        type: 1,
        value: "...",
        selected: false
      };
    }
  }

  getPageItemClass(item: any) {
    if (item.type == 0) {
      if (item.selected)
        return "active";
      else
        return "";
    }
    else {
      return "page-first-separator, disabled"
    }
  }

  checkChanged(changeValue: any, modifyValue: any = 0) {
    changeValue = parseInt(changeValue) + parseInt(modifyValue);
    if (changeValue == this._currentPage)
      return;
    var lastPage = (this._itemTotal % this._pageSize) == 0 ? (this._itemTotal / this._pageSize) : (this._itemTotal / this._pageSize + 1);
    if (lastPage == 0)
      lastPage++;
    if (changeValue > 0 && changeValue <= lastPage) {
      this._currentPage = changeValue;
    }
    else {
      return;
    }
    this.currentPage = changeValue;
    this.refreshItems();
    this.changePage.emit(changeValue);
  }

}
