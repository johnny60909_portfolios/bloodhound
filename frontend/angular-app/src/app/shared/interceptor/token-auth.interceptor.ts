import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { HttpService } from '../services/http.service';
import { SharedService } from '../services/shared.service';

@Injectable()
export class TokenAuthInterceptor implements HttpInterceptor {

  constructor(public sharedService: SharedService, private httpService: HttpService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newRequest = request;
    if (newRequest.headers.get('Authorization') === null) {
      newRequest = request.clone({ setHeaders: { Authorization: 'Bearer ' + this.sharedService.getAuthToken() } });
    }

    return next.handle(newRequest).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          setTimeout(() => {
            this.httpService.loaderShow = false;
          }, 0);
        }
      })
    ).pipe(
      catchError(this.handleError<any>())
    );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      if (error instanceof HttpErrorResponse) {
        setTimeout(() => {
          this.httpService.loaderShow = false;
          if (error.status === 401) {
            this.sharedService.removeTokens();
            this.sharedService.goBackToHome();
          }
          else if (error.status === 400) {
            this.sharedService.showValidatorMsg(error.error.messages);
          }
          else if (error.status === 422) {
            this.sharedService.showValidatorMsg(error.error.messages);
          }
        }, 0);
        return throwError(() => error);
      }
      return of(result as T);
    };
  }

}
