import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: any, format: string): any {
    var time = moment(value, "HH:mm:ss");
    var result = time.format(format);
    return result;
  }

}
