import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http.service';
import { SharedService } from 'src/app/shared/services/shared.service';
import Swal from 'sweetalert2';
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // 新建一個 FormGroup 物件
  loginForm: FormGroup = this.formBuilder.group({
    account: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  constructor(
    private httpService: HttpService,
    public sharedService: SharedService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {

  }

  login(): void {
    this.loginForm.value;
    this.httpService.post('/api/aggregation/Auth/Login', this.loginForm.value).subscribe(res => {
      if (res.succeeded) {
        this.sharedService.user.account = res.user.staffID;
        this.sharedService.user.roleName = res.user.roleName;
        this.sharedService.user.jobTitle = res.user.jobTitle;
        this.sharedService.user.gender = res.user.gender;
        this.sharedService.storeTokens(res.token, res.expiresIn);
        this.sharedService.goBackToHome();
      }
      else {
        Swal.fire({
          icon: 'warning',
          title: '登入失敗',
          showConfirmButton: true,
          scrollbarPadding: false,
        });
      }
    });
  }

}
