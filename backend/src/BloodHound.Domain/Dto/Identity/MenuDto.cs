﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Dto.Identity
{
    public class MenuDto
    {
        /// <summary>
        /// 角色群組可用的程式
        /// </summary>
        public class GroupFunctionDto
        {
            // 群組Id
            public string FunctionGroupId { get; set; } = default!;

            /// <summary>
            /// 程式群組名稱
            /// </summary>
            public string FunctionGroupName { get; set; } = default!;

            public List<SubFunctionDto> SubFunctions { get; set; } = default!;
        }

        /// <summary>
        /// 子功能頁面
        /// </summary>
        public class SubFunctionDto
        {
            /// <summary>
            /// 程式名稱
            /// </summary>
            public string FunctionName { get; set; } = default!;

            /// <summary>
            /// 程式ID
            /// </summary>
            public int FunctionId { get; set; }

            /// <summary>
            /// 排序
            /// </summary>
            public int Sort { get; set; }

            /// <summary>
            /// 程式可操作的功能
            /// </summary>
            public List<EnableOperationDto> EnabledOperations { get; set; } = default!;
        }

        /// <summary>
        /// 功能
        /// </summary>
        public class EnableOperationDto
        {
            /// <summary>
            /// FunctionOperationId
            /// </summary>
            public int FunctionOperationId { get; set; }

            /// <summary>
            /// 操作功能Id
            /// </summary>
            public int OperationId { get; set; }

            /// <summary>
            /// 操作功能名稱
            /// </summary>
            public string OperationName { get; set; } = default!;

            /// <summary>
            /// 操作功能是否啟用
            /// </summary>
            public bool OperationIsEnable { get; set; }
        }

        /// <summary>
        /// 管理網站的菜單群組
        /// </summary>
        public class LayoutMenuGroupDto
        {
            /// <summary>
            /// 程式群組名稱
            /// </summary>
            public string MenuGroupName { get; set; } = default!;

            public string IconClass { get; set; } = default!;

            public List<LayoutMenuSubDto> SubMenus { get; set; } = default!;
        }

        /// <summary>
        /// 管理網站的菜單項目
        /// </summary>
        public class LayoutMenuSubDto
        {
            /// <summary>
            /// 子頁面名稱
            /// </summary>
            public string MenuName { get; set; } = default!;
            /// <summary>
            /// 子頁面ID
            /// </summary>
            public int FunctionId { get; set; }
            /// <summary>
            /// 路由-控制器
            /// </summary>
            public string Controller { get; set; } = default!;
            /// <summary>
            /// 路由-動作
            /// </summary>
            public string Action { get; set; } = default!;
            /// <summary>
            /// 可操作權限
            /// </summary>
            public PermissionDto Permission { get; set; } = default!;
        }

        public class PermissionDto
        {
            /// <summary>
            /// 新增
            /// </summary>
            public bool CanInsert { get; set; } = false;
            /// <summary>
            /// 修改
            /// </summary>
            public bool CanUpdate { get; set; } = false;
            /// <summary>
            /// 刪除
            /// </summary>
            public bool CanDelete { get; set; } = false;
            /// <summary>
            /// 下載
            /// </summary>
            public bool CanDownload { get; set; } = false;
            /// <summary>
            /// 瀏覽
            /// </summary>
            public bool CanBrowse { get; set; } = false;

            public bool Rollback
            {
                get
                {
                    if (CanBrowse)
                        return false;
                    else
                        return true;
                }
            }
        }
    }
}
