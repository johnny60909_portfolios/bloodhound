﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Dto.Identity
{
    public class RoleDto
    {
        public class RoleList
        {
            public string Id { get; set; } = default!;

            /// <summary>
            /// 角色名字
            /// </summary>
            public string Name { get; set; } = default!;

            /// <summary>
            /// 是否啟用
            /// </summary>
            public bool IsEnabled { get; set; }
        }

        public class UpdateEnabled
        {
            public string Id { get; set; } = default!;

            public bool IsEnabled { get; set; }
        }

        public class CreateRole
        {
            /// <summary>
            /// 角色名字
            /// </summary>
            public string Name { get; set; } = default!;

            /// <summary>
            /// 父階角色
            /// </summary>
            public string ParentRoleId { get; set; } = default!;
        }

        public class Role
        {
            /// <summary>
            /// 角色代碼
            /// </summary>
            public string Id { get; set; } = default!;

            /// <summary>
            /// 角色名稱
            /// </summary>
            public string Name { get; set; } = default!;

            /// <summary>
            /// 父階角色
            /// </summary>
            public string ParentRoleId { get; set; } = default!;

            /// <summary>
            /// 角色群組可用的程式功能
            /// </summary>
            public List<MenuDto.GroupFunctionDto> RoleFunctions { get; set; } = default!;
        }
    }
}
