﻿namespace TVGHiHMS.Core.Dto;

public class JsonResponse<T>
{
    public T Result { get; set; }
}

public class JsonTableResponse<T> : JsonResponse<T>
{
    public int Total { get; set; }
}
