﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Dto.Punch
{
    public class PunchDto
    {
        public class Punch
        {
            /// <summary>
            /// 打卡日
            /// </summary>
            public DateTime PushDate { get; set; }

            /// <summary>
            /// 第一次打卡時間
            /// </summary>
            public TimeSpan? FirstTime { get; set; }

            /// <summary>
            /// 最後打卡時間
            /// </summary>
            public TimeSpan? LastTime { get; set; }

            /// <summary>
            /// 打卡時間
            /// </summary>
            public List<TimeSpan> PushTimes { get; set; } = default!;
        }

        public class SearchParams
        {
            /// <summary>
            /// 開始日期
            /// </summary>
            public string StartDate { get; set; } = default!;

            /// <summary>
            /// 結束日期
            /// </summary>
            public string EndDate { get; set; } = default!;

            /// <summary>
            /// 帳號查看
            /// </summary>
            public string UserId { get; set; } = default!;

            public PageDto PageSetting { get; set; } = default!;
        }

        public class PunchList : Punch
        {
            public Guid UserId { get; set; } = default!;
            public string UserName { get; set; } = default!;
        }
    }
}
