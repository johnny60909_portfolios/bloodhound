﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Dto
{
    public class KeyValueDto<T1,T2>
    {
        public T1 Key { get; set; } = default!;

        public T2 Value { get; set; } = default!;
    }
}
