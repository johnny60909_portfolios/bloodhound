﻿namespace BloodHound.Domain.Dto.Web
{
    public class AccountDto
    {
        public class Login
        {
            /// <summary>
            /// 帳號(使用者名稱)
            /// </summary>
            public string Account { get; set; } = default!;

            /// <summary>
            /// 密碼
            /// </summary>
            public string Password { get; set; } = default!;
        }

        public class LoginResult
        {
            public string Token { get; set; } = default!;
            public DateTime? ExpiresIn { get; set; } = default!;
            public bool Succeeded { get; set; } = default!;
            public string Message { get; set; } = default!;             
            public Identity.UserDto.LoginUser User { get; set; } = default!;
        }
    }
}
