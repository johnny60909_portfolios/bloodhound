﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Dto
{
    public class JsonErrorResponse
    {
        public string Messages { get; set; } = default!;
    }
}
