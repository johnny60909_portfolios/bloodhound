﻿namespace BloodHound.Domain.Dto;

public class PageDto
{
    public int? Rows { get; set; }
    public int? Page { get; set; }
    public int TotalCount { get; set; } = 0;
}
