﻿namespace BloodHound.Domain.Const
{
    public class HttpClientConsts
    {
        /// <summary>
        /// Identity API
        /// </summary>
        public const string Identity_API_Name = "Identity";

        /// <summary>
        /// Punch API
        /// </summary>
        public const string Punch_API_Name = "Punch";

        /// <summary>
        /// Saga API
        /// </summary>
        public const string Saga_API_Name = "Saga";
    }
}
