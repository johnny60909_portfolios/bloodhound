﻿namespace BloodHound.Domain.Const;

public class AdminUserConsts
{
    /// <summary>
    /// 系統管理者 帳號
    /// </summary>
    public const string Name = "admin";

    /// <summary>
    /// 系統管理者 密碼
    /// </summary>
    public const string Password = "123456";

    /// <summary>
    /// 系統管理者 角色名稱
    /// </summary>
    public const string RoleName = "系統管理";
}
