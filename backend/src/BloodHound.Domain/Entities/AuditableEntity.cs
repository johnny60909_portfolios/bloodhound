﻿namespace BloodHound.Domain
{
    public abstract class AuditableEntity
    {
        /// <summary>
        /// 建立人員
        /// </summary>
        public Guid? CreateUserId { set; get; }

        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime? CreateDate { set; get; }

        /// <summary>
        /// 建立來源
        /// </summary>
        public string? CreateFromIp { get; set; }

        /// <summary>
        /// 修改人員
        /// </summary>
        public Guid? ModifyUserId { set; get; }

        /// <summary>
        /// 修改時間
        /// </summary>
        public DateTime? ModifyDate { set; get; }

        /// <summary>
        /// 修改來源
        /// </summary>
        public string? ModifyFromIp { get; set; }
    }
}
