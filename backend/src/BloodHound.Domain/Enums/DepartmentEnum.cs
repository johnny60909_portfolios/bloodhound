﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Enums
{
    /// <summary>
    /// 部門
    /// </summary>
    public enum DepartmentEnum : int
    {
        /// <summary>
        /// 材料
        /// </summary>
        Material = 1,

        /// <summary>
        /// 沙龍
        /// </summary>
        Salon = 2,

        /// <summary>
        /// 教學
        /// </summary>
        Teach = 3,
    }
}
