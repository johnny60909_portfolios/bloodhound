﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodHound.Domain.Enums
{
    /// <summary>
    /// 教育程度
    /// </summary>
    public enum EducationLevelEnum
    {
        /// <summary>
        /// 其他
        /// </summary>
        Other = 0,

        /// <summary>
        /// 高中
        /// </summary>
        HighSchool = 1,

        /// <summary>
        /// 學士
        /// </summary>
        Bachelor = 2,

        /// <summary>
        /// 碩士
        /// </summary>
        Master = 3,

        /// <summary>
        /// 博士
        /// </summary>
        Doctor = 4,
    }
}
