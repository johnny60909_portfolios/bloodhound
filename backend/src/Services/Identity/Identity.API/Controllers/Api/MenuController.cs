﻿using BloodHound.Domain.Dto.Identity;
using Identity.API.Services;
using Identity.API.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Identity.API.Controllers.Api
{
    public class MenuController : ApiControllerBase
    {
        private readonly ILogger<MenuController> _logger;
        private readonly IMenuService _menuService;
        public MenuController(
            ILogger<MenuController> logger,
            IMenuService menuService) : base(logger)
        {
            _logger = logger;
            _menuService = menuService;
        }

        /// <summary>
        /// 取得Menu、功能操作資料
        /// </summary>
        [HttpGet]
        [Route("GetMenuAsync")]
        public async Task<List<MenuDto.LayoutMenuGroupDto>> GetMenuAsync()
        {
            return await _menuService.GetMenuAsync(LoginRoleName);
        }
    }
}
