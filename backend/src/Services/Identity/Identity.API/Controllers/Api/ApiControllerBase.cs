﻿using Identity.API.Const;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Identity.API.Controllers.Api
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {
        protected readonly ILogger<ApiControllerBase> _logger;

        protected ApiControllerBase(ILogger<ApiControllerBase> logger)
        {
            _logger = logger;
        }

        protected Guid LoginUserId => Guid.TryParse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value, out Guid id) ? id : Guid.Empty;
        protected string LoginRoleName => User.FindFirst(ClaimTypes.Role)?.Value ?? string.Empty;
        protected string LoginStaffID => User.FindFirst(OwnJwtClaimTypesConst.StaffID)?.Value ?? string.Empty;
    }
}
