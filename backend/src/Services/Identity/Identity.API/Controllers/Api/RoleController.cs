﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;
using BloodHound.Infrastructure.Exceptions;
using Identity.API.Services;
using Identity.API.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using TVGHiHMS.Core.Dto;
using static BloodHound.Infrastructure.DataValidator.Identity.RoleValidator;

namespace Identity.API.Controllers.Api
{
    public class RoleController : ApiControllerBase
    {
        private readonly ILogger<RoleController> _logger;
        private readonly IRoleService _roleService;
        public RoleController(
            ILogger<RoleController> logger,
            IRoleService roleService) : base(logger)
        {
            _logger = logger;
            _roleService = roleService;
        }

        /// <summary>
        /// 取得Role清單
        /// </summary>
        [HttpPost]
        [Route("GetRoleListAsync")]
        public async Task<JsonTableResponse<List<RoleDto.RoleList>>> GetRoleListAsync(PageDto pageSetting)
        {
            var response = new JsonTableResponse<List<RoleDto.RoleList>>();
            response.Result = await _roleService.GetRoleListAsync(pageSetting, LoginRoleName);
            response.Total = pageSetting.TotalCount;
            return response;
        }

        /// <summary>
        /// 取得全部Role Key/Value清單
        /// </summary>
        [HttpGet]
        [Route("GetAllRoleKeyValueAsync")]
        public async Task<List<KeyValuePair<string, string>>> GetAllRoleKeyValueAsync()
        {
            return await _roleService.GetAllRoleKeyValueAsync();
        }

        /// <summary>
        /// 切換Role啟用
        /// </summary>
        [HttpPost]
        [Route("ChangeEnable")]
        public async Task ChangeEnable(RoleDto.UpdateEnabled data)
        {
            await _roleService.ChangeEnable(data, LoginRoleName);
        }

        /// <summary>
        /// 建立Role資料
        /// </summary>
        [HttpPost]
        [Route("CreateRoleAsync")]
        public async Task CreateRoleAsync(RoleDto.CreateRole data)
        {
            #region 驗證
            var validator = new CreateRoleValidator();
            FluentValidation.Results.ValidationResult validationResult = validator.Validate(data);
            if (!validationResult.IsValid)
            {
                var msg = validationResult.ToString("<br>");
                throw new ValidatorException(msg);
            }
            #endregion

            await _roleService.CreateRoleAsync(data, LoginRoleName);
        }

        /// <summary>
        /// 取得Role資料
        /// </summary>
        [HttpGet]
        [Route("GetRoleAsync")]
        public async Task<RoleDto.Role> GetRoleAsync(string id)
        {
            return await _roleService.GetRoleAsync(id, LoginRoleName);
        }

        /// <summary>
        /// 編輯Role資料
        /// </summary>
        [HttpPost]
        [Route("EditRoleAsync")]
        public async Task EditRoleAsync(RoleDto.Role data)
        {           
            await _roleService.EditRoleAsync(data, LoginRoleName);
        }

    }
}
