﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;
using BloodHound.Infrastructure.Exceptions;
using FluentValidation;
using Identity.API.Services;
using Identity.API.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TVGHiHMS.Core.Dto;
using static BloodHound.Infrastructure.DataValidator.Identity.UserValidator;

namespace Identity.API.Controllers.Api
{
    public class UserController : ApiControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        public UserController(
            ILogger<UserController> logger,
            IUserService userService) : base(logger)
        {
            _logger = logger;
            _userService = userService;
        }

        /// <summary>
        /// 取得登入User資料
        /// </summary>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetLoginUserAsync")]
        public async Task<UserDto.LoginUser> GetLoginUserAsync(string staffID)
        {
            if (!string.IsNullOrEmpty(LoginStaffID) && string.IsNullOrEmpty(staffID))
                return await _userService.GetLoginUserAsync(LoginStaffID);

            return await _userService.GetLoginUserAsync(staffID);
        }

        /// <summary>
        /// 取得User資料
        /// </summary>
        [HttpGet]
        [Route("GetUserAsync")]
        public async Task<UserDto.User> GetUserAsync(Guid id)
        {
            return await _userService.GetUserAsync(id);
        }

        /// <summary>
        /// 取得User清單
        /// </summary>
        [HttpPost]
        [Route("GetUserListAsync")]
        public async Task<JsonTableResponse<List<UserDto.UserList>>> GetUserListAsync(PageDto pageSetting)
        {
            var response = new JsonTableResponse<List<UserDto.UserList>>();
            response.Result = await _userService.GetUserListAsync(pageSetting, LoginRoleName);
            response.Total = pageSetting.TotalCount;
            return response;
        }

        /// <summary>
        /// 建立User資料
        /// </summary>
        [HttpPost]
        [Route("CreateUserAsync")]
        public async Task CreateUserAsync(UserDto.EditUser data)
        {
            #region 驗證
            var validator = new CreateUserValidator();
            FluentValidation.Results.ValidationResult validationResult = validator.Validate(data);
            if (!validationResult.IsValid)
            {
                var msg = validationResult.ToString("<br>");
                throw new ValidatorException(msg);
            }
            #endregion

            await _userService.CreateUserAsync(data, LoginRoleName);
        }

        /// <summary>
        /// 編輯User資料
        /// </summary>
        [HttpPut]
        [Route("EditUserAsync")]
        public async Task EditUserAsync(UserDto.EditUser data)
        {
            #region 驗證
            var validator = new EditUserListValidator();
            FluentValidation.Results.ValidationResult validationResult = validator.Validate(data);
            if (!validationResult.IsValid)
            {
                var msg = validationResult.ToString("<br>");
                throw new ValidatorException(msg);
            }
            #endregion

            await _userService.EditUserAsync(data, LoginRoleName);
        }

        /// <summary>
        /// 切換User啟用
        /// </summary>
        [HttpPost]
        [Route("ChangeEnable")]
        public async Task ChangeEnable(UserDto.UpdateEnabled data)
        {
            await _userService.ChangeEnable(data, LoginRoleName);
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        [HttpPost]
        [Route("ChangePassword")]
        public async Task ChangePassword(UserDto.UpdatePassword data)
        {
            #region 驗證
            var validator = new UpdatePasswordValidator();
            FluentValidation.Results.ValidationResult validationResult = validator.Validate(data);
            if (!validationResult.IsValid)
            {
                var msg = validationResult.ToString("<br>");
                throw new ValidatorException(msg);
            }
            #endregion
            await _userService.ChangePassword(data, LoginRoleName);
        }

        /// <summary>
        /// 取得全部User Key/Value清單
        /// </summary>
        [HttpGet]
        [Route("GetAllUserKeyValueAsync")]
        public async Task<List<KeyValuePair<string, string>>> GetAllUserKeyValueAsync()
        {
            return await _userService.GetAllUserKeyValueAsync();
        }

    }
}
