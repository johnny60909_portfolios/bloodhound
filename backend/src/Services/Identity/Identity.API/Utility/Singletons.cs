﻿
namespace Identity.API.Utility
{
    public static class Singletons
    {
        /// <summary>
        /// 取得員工編號的lock
        /// </summary>
        public static readonly object StaffIDNumberLock = new object();
    }
}