﻿using IdentityServer4;
using IdentityServer4.Models;

namespace Identity.API.Configuration
{
    public class Config
    {
        // ApiResources define the apis in your system
        public static IEnumerable<ApiResource> GetApiResources(IConfiguration configuration)
        {
            return new List<ApiResource>
            {
                new ApiResource(configuration["AuthServer:Scope"], "api Service")
                {
                    Scopes = { configuration["AuthServer:Scope"] }
                },
            };
        }

        public static IEnumerable<ApiScope> GetApiScopes(IConfiguration configuration)
        {
            return new List<ApiScope>
            {
                new ApiScope(configuration["AuthServer:Scope"])
            };
        }

        // see: http://docs.identityserver.io/en/release/configuration/resources.html
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            return new List<Client>
            {
                // JavaScript Client
                new Client
                {
                    ClientId = configuration["AuthServer:ClientId"],
                    ClientName = "SPA OpenId Client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                    //Used to retrieve the access token on the back channel.
                    ClientSecrets =
                    {
                        new Secret(configuration["AuthServer:ClientSecret"].Sha256())
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        configuration["AuthServer:Scope"],
                    },
                    AccessTokenLifetime = 60 * configuration.GetValue<int>("TokenLifetimeMinutes"),
                    IdentityTokenLifetime= 60 * configuration.GetValue<int>("TokenLifetimeMinutes"),
                },
            };
        }
    }
}
