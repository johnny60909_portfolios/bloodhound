﻿using BloodHound.Domain.Dto.Identity;
using Identity.API.Data;
using Identity.API.Services.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Identity.API.Services
{
    public class MenuService : ServiceBase, IMenuService
    {
        public MenuService(
            ILogger<MenuService> logger,
            ApplicationDbContext db
            ) : base(logger, db)
        {

        }

        public Task<List<MenuDto.LayoutMenuGroupDto>> GetMenuAsync(string loginRoleName)
        {
            var roleOperation = _db.RolePermission
                .Include(a => a.Role)
                .Include(a => a.FunctionOperation)
                    .ThenInclude(a => a.Operation)
                .Where(a => a.Role.Name == loginRoleName && a.OpIsEnable)
                .ToList();

            var roleOperationIds = roleOperation.Select(a => a.FunctionOperationId);

            var result = _db.FunctionOperation
                .Include(a => a.Function)
                    .ThenInclude(a => a.FunctionGroup)
                .Where(a => roleOperationIds.Contains(a.FunctionOperationId))
                .ToList()
                .GroupBy(a => new { a.Function.FunctionGroup.FunctionGroupId, a.Function.FunctionGroup.Name, a.Function.FunctionGroup.IconClass, a.Function.FunctionGroup.Seq })
                .OrderBy(a => a.Key.Seq)
                .Select(a => new MenuDto.LayoutMenuGroupDto
                {
                    MenuGroupName = a.Key.Name,
                    IconClass = a.Key.IconClass,
                    SubMenus =
                        a.OrderBy(f => f.Function.Seq).Select(f => new MenuDto.LayoutMenuSubDto
                        {
                            FunctionId = f.FunctionId,
                            MenuName = f.Function.Name,
                            Controller = f.Function.Controller,
                            Action = f.Function.Action.Split(',')[0],
                        })
                        .GroupBy(f => new { f.FunctionId, f.MenuName, f.Controller, f.Action })
                        .Select(f => new MenuDto.LayoutMenuSubDto
                        {
                            FunctionId = f.Key.FunctionId,
                            MenuName = f.Key.MenuName,
                            Controller = f.Key.Controller,
                            Action = f.Key.Action,
                            Permission = new MenuDto.PermissionDto
                            {
                                CanInsert = roleOperation
                                    .Where(b => b.FunctionOperation.FunctionId == f.Key.FunctionId)
                                    .Any(b => b.FunctionOperation.Operation.OperationId == 1),
                                CanUpdate = roleOperation
                                    .Where(b => b.FunctionOperation.FunctionId == f.Key.FunctionId)
                                    .Any(b => b.FunctionOperation.Operation.OperationId == 2),
                                CanDelete = roleOperation
                                    .Where(b => b.FunctionOperation.FunctionId == f.Key.FunctionId)
                                    .Any(b => b.FunctionOperation.Operation.OperationId == 3),
                                CanBrowse = roleOperation
                                    .Where(b => b.FunctionOperation.FunctionId == f.Key.FunctionId)
                                    .Any(b => b.FunctionOperation.Operation.OperationId == 4),
                                CanDownload = roleOperation
                                    .Where(b => b.FunctionOperation.FunctionId == f.Key.FunctionId)
                                    .Any(b => b.FunctionOperation.Operation.OperationId == 5),
                            }
                        })
                        .ToList()
                }).ToList();

            return Task.FromResult(result);
        }
    }
}
