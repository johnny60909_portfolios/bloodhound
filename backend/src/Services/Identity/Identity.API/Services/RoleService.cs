﻿using BloodHound.Domain.Const;
using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;
using BloodHound.Infrastructure.Exceptions;
using Identity.API.Data;
using Identity.API.Entities;
using Identity.API.Services.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Identity.API.Services
{
    public class RoleService : ServiceBase, IRoleService
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public RoleService(
            ILogger<RoleService> logger,
            ApplicationDbContext db,
            RoleManager<ApplicationRole> roleManager,
            UserManager<ApplicationUser> userManager
            ) : base(logger, db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public Task<List<RoleDto.RoleList>> GetRoleListAsync(PageDto pageSetting, string loginRoleName)
        {
            var filterRoles = base.GetChildRoleKeyValue(loginRoleName, _roleManager);

            var query = _roleManager.Roles
                .Where(a => filterRoles.Select(b => b.Key).Contains(a.Id))
                .AsQueryable();

            if (pageSetting != null)
            {
                pageSetting.TotalCount = query.Count();
                if (pageSetting.Page.HasValue
                    && pageSetting.Page.Value > 0
                    && pageSetting.Rows.HasValue
                    && pageSetting.Rows.Value > 0)
                {
                    query = query
                        .Skip((pageSetting.Rows.Value - 1) * pageSetting.Page.Value)
                        .Take(pageSetting.Page.Value);
                }
            }

            var result = query.ToList()
                .Select(a => new RoleDto.RoleList
                {
                    Id = a.Id.ToString().ToLower(),
                    Name = a.Name,
                    IsEnabled = a.IsEnabled
                }).ToList();

            return Task.FromResult(result);
        }

        public Task<List<KeyValuePair<string, string>>> GetAllRoleKeyValueAsync()
        {
            var result = _roleManager.Roles
                .Select(a => new KeyValuePair<string, string>(a.Id.ToString().ToLower(), a.Name))
                .ToList();
            return Task.FromResult(result);
        }

        public async Task ChangeEnable(RoleDto.UpdateEnabled data, string loginRoleName)
        {
            Guid.TryParse(data.Id, out var id);

            var role = _roleManager.Roles
                .Include(a => a.UserRoles)
                .FirstOrDefault(a => a.Id == id);

            if (role == null)
                throw new DomainException("找不到此角色");

            if (role.Name == AdminUserConsts.RoleName)
                throw new DomainException("系統角色不可變更");

            if (!base.CheckRoleHierarchy(loginRoleName, role.Id, _roleManager))
                throw new DomainException("角色權限不足");

            role.IsEnabled = data.IsEnabled;

            await _roleManager.UpdateAsync(role);
        }

        public async Task CreateRoleAsync(RoleDto.CreateRole data, string loginRoleName)
        {
            Guid.TryParse(data.ParentRoleId, out Guid parentRoleId);

            var parentRole = _roleManager.Roles
                .Include(a => a.UserRoles)
                .FirstOrDefault(a => a.Id == parentRoleId);

            if (parentRole == null)
                throw new DomainException("找不到此父階角色");

            var roleExistResult = await _roleManager.RoleExistsAsync(data.Name);
            if (roleExistResult)
            {
                throw new DomainException("該角色已存在");
            }

            if (!base.CheckRoleHierarchy(loginRoleName, parentRole.Id, _roleManager))
                throw new DomainException("角色權限不足");

            var role = new ApplicationRole()
            {
                Name = data.Name,
                IsEnabled = true,
                ParentRoleRelations = new List<ApplicationRoleRelation>()
                {
                    new ApplicationRoleRelation
                    {
                        ParentId = parentRole.Id
                    }
                }
            };

            var roleAddResult = await _roleManager.CreateAsync(role);

            if (!roleAddResult.Succeeded)
                throw new DomainException("角色新增失敗");
        }

        public Task<RoleDto.Role> GetRoleAsync(string id, string loginRoleName)
        {
            Guid.TryParse(id, out var guidId);

            var role = _roleManager.Roles
                .Include(r => r.RolePermissions)
                    .ThenInclude(rp => rp.FunctionOperation.Function.FunctionGroup)
                .Include(r => r.RolePermissions)
                    .ThenInclude(rp => rp.FunctionOperation.Operation)
                .Include(r => r.ParentRoleRelations)
                .FirstOrDefault(r => r.Id == guidId);

            if (role == null)
                throw new DomainException("找不到此角色");

            var allFunctions = _db.Function
                .Include(f => f.FunctionGroup)
                .Include(f => f.FunctionOperations)
                    .ThenInclude(fo => fo.Operation)
                .ToList()
                .GroupBy(f => new { f.FunctionGroup.FunctionGroupId, f.FunctionGroup.Name, f.FunctionGroup.Seq })
                .OrderBy(fg => fg.Key.Seq)
                .Select(fg => new MenuDto.GroupFunctionDto
                {

                    FunctionGroupId = fg.Key.FunctionGroupId.ToString(),
                    FunctionGroupName = fg.Key.Name,
                    SubFunctions =
                            fg.Select(f => new MenuDto.SubFunctionDto
                            {
                                FunctionId = f.FunctionId,
                                FunctionName = f.Name,
                                Sort = f.Seq.HasValue ? f.Seq.Value : 99,
                                EnabledOperations = f.FunctionOperations.OrderBy(a => a.OperationId)
                                .Select(fo => new MenuDto.EnableOperationDto
                                {
                                    FunctionOperationId = fo.FunctionOperationId,
                                    OperationId = fo.OperationId,
                                    OperationIsEnable = false,
                                    OperationName = fo.Operation.Name,
                                }).ToList()
                            }).OrderBy(f => f.Sort).ToList()
                })
                .ToList();

            var roleFunctions = role.RolePermissions
                    .GroupBy(p => p.FunctionOperation.Function.FunctionGroup)
                    .Select(d => new MenuDto.GroupFunctionDto
                    {
                        FunctionGroupName = d.Key.Name,
                        SubFunctions =
                            d.GroupBy(p => p.FunctionOperation.Function)
                            .Select(f => new MenuDto.SubFunctionDto
                            {
                                FunctionId = f.Key.FunctionId,
                                FunctionName = f.Key.Name,
                                EnabledOperations = f.Select(p => new MenuDto.EnableOperationDto
                                {
                                    FunctionOperationId = p.FunctionOperationId,
                                    OperationId = p.FunctionOperation.OperationId,
                                    OperationIsEnable = p.OpIsEnable,
                                    OperationName = p.FunctionOperation.Operation.Name
                                }).ToList()
                            }).OrderBy(f => f.FunctionName).ToList()
                    }).ToList();

            foreach (var fg in allFunctions)
            {
                foreach (var sf in fg.SubFunctions)
                {
                    foreach (var op in sf.EnabledOperations)
                    {
                        op.OperationIsEnable = role.RolePermissions
                            .FirstOrDefault(a => a.FunctionOperationId == op.FunctionOperationId)?.OpIsEnable ?? false;
                    }
                }
            }

            var result = new RoleDto.Role
            {
                Id = role.Id.ToString().ToLower(),
                Name = role.Name,
                RoleFunctions = allFunctions,
                ParentRoleId = role.ParentRoleRelations
                    .Where(x => x.ChildId == guidId)
                    .Select(x => x.ParentId.ToString())
                    .FirstOrDefault()
            };

            return Task.FromResult(result);
        }

        public async Task EditRoleAsync(RoleDto.Role data, string loginRoleName)
        {
            Guid.TryParse(data.Id, out var roleId);
            Guid.TryParse(data.ParentRoleId, out var parentRoleId);

            var functionOperationId_Enable = new List<string>();

            var role = _roleManager.Roles
                .Include(r => r.RolePermissions)
                .Include(r => r.ParentRoleRelations)
                .FirstOrDefault(r => r.Id == roleId);

            if (role == null)
                throw new DomainException("找不到此角色");

            if (role.Name == AdminUserConsts.RoleName)
                throw new DomainException("系統角色不可變更");

            var parentRole = _roleManager.Roles
                .FirstOrDefault(r => r.Id == parentRoleId);

            if (parentRole == null)
                throw new DomainException("找不到此父階角色");

            if (parentRole.Id == role.Id)
                throw new DomainException("父階角色不可與此角色相同");

            //檢查父階角色權限異動            
            var parentRoleIds = _roleManager.Roles
                .Include(a => a.ParentRoleRelations)
                    .ThenInclude(a => a.ParentRole)
                .First(a => a.Name == loginRoleName).ParentRoleRelations?
                .Select(a => a.ParentRole.Id);
            if (parentRoleIds != null && parentRoleIds.Any())
            {
                if (!parentRoleIds.Contains(parentRole.Id))
                    throw new DomainException("角色權限不足");
            }

            var rolePermissions = role.RolePermissions;

            var loaclData = data.RoleFunctions
                .SelectMany(a => a.SubFunctions, (a, b) => b)
                .SelectMany(a => a.EnabledOperations, (b, c) => c)
                .Where(a => a.OperationIsEnable)
                .Select(a => new RolePermission
                {
                    RoleId = role.Id,
                    FunctionOperationId = a.FunctionOperationId,
                    OpIsEnable = true
                });

            var insertEntity = loaclData.Except(rolePermissions, new RolePermissionComparer()).ToList();
            var deleteEntity = rolePermissions.Except(loaclData, new RolePermissionComparer()).ToList();

            _db.RolePermission.RemoveRange(deleteEntity);
            _db.RolePermission.AddRange(insertEntity);

            var functionOperations = _db.FunctionOperation
                .Select(x => x.FunctionOperationId)
                .ToList();

            //寫入父階角色
            if (!string.IsNullOrEmpty(data.ParentRoleId))
            {
                role.ParentRoleRelations?.Clear();
                role.ParentRoleRelations?.Add(new ApplicationRoleRelation
                {
                    ParentId = parentRoleId
                });
            }

            await _roleManager.UpdateAsync(role);

            _db.SaveChanges();
        }

        public class RolePermissionComparer : IEqualityComparer<RolePermission>
        {
            public bool Equals(RolePermission x, RolePermission y)
            {
                return x.RoleId == y.RoleId && x.FunctionOperationId == y.FunctionOperationId;
            }

            public int GetHashCode(RolePermission bx)
            {
                return $"{bx.RoleId}_{bx.FunctionOperationId}".GetHashCode();
            }
        }
    }
}
