﻿using BloodHound.Domain.Dto.Identity;

namespace Identity.API.Services.Interface
{
    public interface IMenuService
    {
        Task<List<MenuDto.LayoutMenuGroupDto>> GetMenuAsync(string loginRoleName);
    }
}
