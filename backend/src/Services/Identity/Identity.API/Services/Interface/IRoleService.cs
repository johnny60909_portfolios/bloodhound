﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;

namespace Identity.API.Services.Interface
{
    public interface IRoleService
    {
        Task ChangeEnable(RoleDto.UpdateEnabled data, string loginRoleName);
        Task CreateRoleAsync(RoleDto.CreateRole data, string loginRoleName);
        Task EditRoleAsync(RoleDto.Role data, string loginRoleName);
        Task<List<KeyValuePair<string, string>>> GetAllRoleKeyValueAsync();
        Task<RoleDto.Role> GetRoleAsync(string id, string loginRoleName);
        Task<List<RoleDto.RoleList>> GetRoleListAsync(PageDto pageSetting, string loginRoleName);
    }
}
