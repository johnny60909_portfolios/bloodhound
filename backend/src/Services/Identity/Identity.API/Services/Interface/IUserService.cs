﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;

namespace Identity.API.Services.Interface
{
    public interface IUserService
    {
        Task ChangeEnable(UserDto.UpdateEnabled data, string loginRoleName);
        Task ChangePassword(UserDto.UpdatePassword data, string loginRoleName);
        Task CreateUserAsync(UserDto.EditUser data, string loginRoleName);
        Task EditUserAsync(UserDto.EditUser data, string loginRoleName);
        Task<List<KeyValuePair<string, string>>> GetAllUserKeyValueAsync();
        Task<UserDto.User> GetUserAsync(Guid id);
        Task<UserDto.LoginUser> GetLoginUserAsync(string staffID);
        Task<List<UserDto.UserList>> GetUserListAsync(PageDto pageSetting, string loginRoleName);
    }
}
