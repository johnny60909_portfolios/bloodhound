﻿using Identity.API.Data;
using Identity.API.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NLog.Targets;

namespace Identity.API.Services
{
    public class ServiceBase
    {
        protected readonly ILogger<ServiceBase> _logger;
        protected readonly ApplicationDbContext _db;

        public ServiceBase(ILogger<ServiceBase> logger, ApplicationDbContext db)
        {
            _logger = logger;
            _db = db;
        }

        protected bool CheckRoleHierarchy(string myRoleName, Guid targetRoleId, RoleManager<ApplicationRole> roleManager)
        {
            var role = roleManager.Roles.First(a => a.Name == myRoleName);
            var filterRole = GetChildRoleKeyValue(role.Id, roleManager);
            return filterRole.Select(a => a.Key).Contains(targetRoleId);
        }

        protected List<KeyValuePair<Guid, string>> GetChildRoleKeyValue(string roleName, RoleManager<ApplicationRole> roleManager)
        {
            var role = roleManager.Roles.First(a => a.Name == roleName);
            return GetChildRoleKeyValue(role.Id, roleManager);
        }

        protected List<KeyValuePair<Guid, string>> GetChildRoleKeyValue(Guid roleId, RoleManager<ApplicationRole> roleManager)
        {
            var roleList = roleManager.Roles
                .Include(a => a.ChildRoleRelations)
                    .ThenInclude(a => a.ChildRole)
                .ToList();

            var result = new List<KeyValuePair<Guid, string>>();

            var role = roleList.First(a => a.Id == roleId);

            result.Add(new KeyValuePair<Guid, string>(role.Id, role.Name));

            GetSubRoleKeyValue(ref result, role.Id, roleList);

            return result.Distinct().ToList();
        }

        private void GetSubRoleKeyValue(ref List<KeyValuePair<Guid, string>> result, Guid parentId, List<ApplicationRole> roles)
        {
            var childRoleRelations = roles.First(a => a.Id == parentId).ChildRoleRelations.Where(a => a.ParentId == parentId && a.ChildId != parentId).ToList();
            foreach (var roleRelation in childRoleRelations)
            {
                var role = roles.First(a => a.Id == roleRelation.ChildId);
                result.Add(new KeyValuePair<Guid, string>(role.Id, role.Name));
                GetSubRoleKeyValue(ref result, role.Id, roles);
            }
        }
    }
}
