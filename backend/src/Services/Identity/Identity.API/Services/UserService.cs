﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;
using BloodHound.Domain.Enums;
using BloodHound.Infrastructure.Exceptions;
using Identity.API.Data;
using Identity.API.Entities;
using Identity.API.Services.Interface;
using Identity.API.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Identity.API.Services
{
    public class UserService : ServiceBase, IUserService
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserService(
            ILogger<UserService> logger,
            ApplicationDbContext db,
            RoleManager<ApplicationRole> roleManager,
            UserManager<ApplicationUser> userManager
            ) : base(logger, db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public Task<UserDto.LoginUser> GetLoginUserAsync(string staffID)
        {
            var user = _userManager.Users
                .Include(a => a.UserRoles)
                    .ThenInclude(a => a.Role)
                .FirstOrDefault(a => a.StaffID == staffID);
            if (user != null)
            {
                var result = new UserDto.LoginUser();
                result.Account = user.UserName;
                result.StaffID = user.StaffID;
                result.Name = user.Name;
                result.RoleName = user.UserRoles?.FirstOrDefault()?.Role?.Name.ToString().ToLower() ?? string.Empty;
                result.JobTitle = user.JobTitle;
                result.Gender = user.Gender;
                return Task.FromResult(result);
            }
            else
                throw new DomainException("找不到此帳號");
        }

        public Task<UserDto.User> GetUserAsync(Guid id)
        {
            var user = _userManager.Users
                .Include(a => a.UserRoles)
                    .ThenInclude(a => a.Role)
                .FirstOrDefault(a => a.Id == id);

            if (user != null)
            {
                var result = new UserDto.User();
                result.Id = user.Id.ToString().ToLower();
                result.Account = user.UserName;
                result.RoleId = user.UserRoles?.FirstOrDefault()?.Role?.Id.ToString().ToLower() ?? string.Empty;
                result.Name = user.Name;
                result.EnglishName = user.EnglishName;
                result.Department = user.Department.HasValue ? ((int)user.Department.Value).ToString() : string.Empty;
                result.StaffID = user.StaffID;
                result.OnBoardDate = user.OnBoardDate.HasValue ? user.OnBoardDate.Value.ToString("yyyy-MM-dd") : string.Empty;
                result.JobTitle = user.JobTitle;
                result.Gender = user.Gender;
                result.IsMarried = user.IsMarried;
                result.IsNationality = user.IsNationality;
                result.IdentityNo = user.IdentityNo;
                result.ResidentVisaNumber = user.ResidentVisaNumber;
                result.PassportNumber = user.PassportNumber;
                result.Birthday = user.Birthday.HasValue ? user.Birthday.Value.ToString("yyyy-MM-dd") : string.Empty;
                result.Telephone = user.Telephone;
                result.CurrentAddress = user.CurrentAddress;
                result.ResidenceAddress = user.ResidenceAddress;
                result.EducationLevel = user.EducationLevel.HasValue ? ((int)user.EducationLevel.Value).ToString() : string.Empty;
                result.EducationLevelOther = user.EducationLevelOther;
                result.EmergencyContactPerson = user.EmergencyContactPerson;
                result.EmergencyContactPersonRelation = user.EmergencyContactPersonRelation;
                result.ContactPhone = user.ContactPhone;
                result.ResignationDate = user.ResignationDate.HasValue ? user.ResignationDate.Value.ToString("yyyy-MM-dd") : string.Empty;

                return Task.FromResult(result);
            }
            else
                throw new DomainException("找不到此帳號");
        }

        public Task<List<UserDto.UserList>> GetUserListAsync(PageDto pageSetting, string loginRoleName)
        {
            var filterRoles = base.GetChildRoleKeyValue(loginRoleName, _roleManager);

            var query = _userManager.Users
                    .Include(u => u.UserRoles)
                        .ThenInclude(u => u.Role)
                    .Where(a => a.UserRoles.Any(b => filterRoles.Select(c => c.Key).Contains(b.RoleId)));

            if (pageSetting != null)
            {
                pageSetting.TotalCount = query.Count();
                if (pageSetting.Page.HasValue
                    && pageSetting.Page.Value > 0
                    && pageSetting.Rows.HasValue
                    && pageSetting.Rows.Value > 0)
                {
                    query = query
                        .Skip((pageSetting.Rows.Value - 1) * pageSetting.Page.Value)
                        .Take(pageSetting.Page.Value);
                }
            }

            var result = query.ToList()
                .Select(a => new UserDto.UserList
                {
                    Id = a.Id.ToString().ToLower(),
                    StaffID = a.StaffID,
                    Name = a.Name,
                    RoleName = a.UserRoles.Any() ? a.UserRoles.First().Role.Name : string.Empty,
                    IsEnabled = a.IsEnabled,
                })
                .ToList();

            return Task.FromResult(result);
        }

        public async Task CreateUserAsync(UserDto.EditUser data, string loginRoleName)
        {
            var role = _roleManager.FindByIdAsync(data.RoleId.ToString()).Result;

            if (!base.CheckRoleHierarchy(loginRoleName, role.Id, _roleManager))
                throw new DomainException("角色權限不足");

            if (string.IsNullOrEmpty(data.Department))
                throw new DomainException("部門不可為空");

            ApplicationUser user = new ApplicationUser();
            user.Id = Guid.NewGuid();
            user.UserName = user.Id.ToString().ToLower();
            user.Name = data.Name;
            user.EnglishName = data.EnglishName;
            user.Department = (DepartmentEnum)int.Parse(data.Department);
            if (!string.IsNullOrEmpty(data.OnBoardDate))
                user.OnBoardDate = DateTime.Parse(data.OnBoardDate);
            user.JobTitle = data.JobTitle;
            user.Gender = data.Gender;
            user.IsMarried = data.IsMarried;
            user.IsNationality = data.IsNationality;
            user.IdentityNo = data.IdentityNo;
            user.ResidentVisaNumber = data.ResidentVisaNumber;
            user.PassportNumber = data.PassportNumber;
            if (!string.IsNullOrEmpty(data.Birthday))
                user.Birthday = DateTime.Parse(data.Birthday);
            user.Telephone = data.Telephone;
            user.CurrentAddress = data.CurrentAddress;
            user.ResidenceAddress = data.ResidenceAddress;
            if (!string.IsNullOrEmpty(data.EducationLevel))
                user.EducationLevel = (EducationLevelEnum)int.Parse(data.EducationLevel);
            user.EducationLevelOther = data.EducationLevelOther;
            user.EmergencyContactPerson = data.EmergencyContactPerson;
            user.EmergencyContactPersonRelation = data.EmergencyContactPersonRelation;
            user.ContactPhone = data.ContactPhone;
            if (!string.IsNullOrEmpty(data.ResignationDate))
                user.ResignationDate = DateTime.Parse(data.ResignationDate);
            user.IsEnabled = true;
            data.Password = data.Password.Trim();

            var validateResult = await _userManager.PasswordValidators //驗證密碼規則
                .FirstOrDefault()
                .ValidateAsync(_userManager, user, data.Password);

            if (!validateResult.Succeeded)
                throw new DomainException(string.Join(',', validateResult.Errors.Select(x => x.Description).ToArray()).Replace(",", "<br>"));

            user.StaffID = GenerateStaffID(user.Department.Value);

            var result = await _userManager.CreateAsync(user, data.Password);
            if (result.Succeeded)
            {
                var addRoleResult = await _userManager.AddToRoleAsync(user, role.Name);
                if (!addRoleResult.Succeeded)
                    throw new Exception("帳號新增失敗");
            }
            else
                throw new Exception("帳號新增失敗");
        }

        public async Task EditUserAsync(UserDto.EditUser data, string loginRoleName)
        {
            Guid.TryParse(data.Id, out var id);

            var role = await _roleManager.FindByIdAsync(data.RoleId.ToString());

            var user = _userManager.Users
                .Include(a => a.UserRoles)
                    .ThenInclude(a => a.Role)
                .FirstOrDefault(a => a.Id == id);

            if (user == null)
                throw new DomainException("找不到此帳號");

            if (!base.CheckRoleHierarchy(loginRoleName, role.Id, _roleManager))
                throw new DomainException("角色權限不足");

            user.Name = data.Name;
            user.EnglishName = data.EnglishName;
            if (!string.IsNullOrEmpty(data.Department))
                user.Department = (DepartmentEnum)int.Parse(data.Department);
            if (!string.IsNullOrEmpty(data.OnBoardDate))
                user.OnBoardDate = DateTime.Parse(data.OnBoardDate);
            user.JobTitle = data.JobTitle;
            user.Gender = data.Gender;
            user.IsMarried = data.IsMarried;
            user.IsNationality = data.IsNationality;
            user.IdentityNo = data.IdentityNo;
            user.ResidentVisaNumber = data.ResidentVisaNumber;
            user.PassportNumber = data.PassportNumber;
            if (!string.IsNullOrEmpty(data.Birthday))
                user.Birthday = DateTime.Parse(data.Birthday);
            user.Telephone = data.Telephone;
            user.CurrentAddress = data.CurrentAddress;
            user.ResidenceAddress = data.ResidenceAddress;
            if (!string.IsNullOrEmpty(data.EducationLevel))
                user.EducationLevel = (EducationLevelEnum)int.Parse(data.EducationLevel);
            user.EducationLevelOther = data.EducationLevelOther;
            user.EmergencyContactPerson = data.EmergencyContactPerson;
            user.EmergencyContactPersonRelation = data.EmergencyContactPersonRelation;
            user.ContactPhone = data.ContactPhone;
            if (!string.IsNullOrEmpty(data.ResignationDate))
                user.ResignationDate = DateTime.Parse(data.ResignationDate);

            var roles = await _userManager.GetRolesAsync(user);
            var removeRoleResult = await _userManager.RemoveFromRolesAsync(user, roles);
            if (removeRoleResult.Succeeded)
            {
                var addRoleResult = await _userManager.AddToRoleAsync(user, role.Name);
                if (!addRoleResult.Succeeded)
                    throw new DomainException("帳號編輯失敗");
            }

            await _userManager.UpdateAsync(user);
        }

        public async Task ChangeEnable(UserDto.UpdateEnabled data, string loginRoleName)
        {
            Guid.TryParse(data.Id, out var id);

            var user = _userManager.Users
                .Include(a => a.UserRoles)
                    .ThenInclude(a => a.Role)
                .FirstOrDefault(a => a.Id == id);

            if (user == null)
                throw new DomainException("找不到此帳號");

            if (!base.CheckRoleHierarchy(loginRoleName, user.UserRoles.First().RoleId, _roleManager))
                throw new DomainException("角色權限不足");

            user.IsEnabled = data.IsEnabled;

            await _userManager.UpdateAsync(user);
        }

        public async Task ChangePassword(UserDto.UpdatePassword data, string loginRoleName)
        {
            Guid.TryParse(data.Id, out var id);

            data.Password = data.Password?.Trim();

            var user = _userManager.Users
                .Include(a => a.UserRoles)
                    .ThenInclude(a => a.Role)
                .FirstOrDefault(a => a.Id == id);

            if (user == null)
                throw new DomainException("找不到此帳號");

            if (!base.CheckRoleHierarchy(loginRoleName, user.UserRoles.First().RoleId, _roleManager))
                throw new DomainException("角色權限不足");

            // 驗證密碼是否符合規則
            var validateResult = await _userManager.PasswordValidators
                .FirstOrDefault()
                .ValidateAsync(_userManager, null, data.Password);

            if (validateResult.Succeeded)
            {
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(null, data.Password);
                var updateUserResult = await _userManager.UpdateAsync(user);
                if (!updateUserResult.Succeeded)
                    throw new Exception("密碼更新失敗");
            }
            else
            {
                var msg = string.Join(',', validateResult.Errors.Select(x => x.Description).ToArray()).Replace(",", "<br>");
                throw new ValidatorException(msg);
            }
        }

        public Task<List<KeyValuePair<string, string>>> GetAllUserKeyValueAsync()
        {
            var result = _userManager.Users
                .Select(a => new KeyValuePair<string, string>(a.Id.ToString().ToLower(), a.Name))
                .ToList();
            return Task.FromResult(result);
        }

        private string GenerateStaffID(DepartmentEnum department)
        {
            lock (Singletons.StaffIDNumberLock)
            {
                var staffIDNumber = _db.StaffIDNumber
                    .Where(x => x.Department == department)
                    .FirstOrDefault();

                if (staffIDNumber == null)
                {
                    string startString = department switch
                    {
                        DepartmentEnum.Material => "A",
                        DepartmentEnum.Salon => "B",
                        DepartmentEnum.Teach => "C",
                        _ => string.Empty,
                    };

                    _db.StaffIDNumber.Add(new StaffIDNumber
                    {
                        Department = department,
                        StartString = startString,
                        SerialNumber = 1
                    });

                    _db.SaveChanges();

                    return $"{startString}{"1".PadLeft(4, '0')}";
                }
                else
                {
                    staffIDNumber.SerialNumber = staffIDNumber.SerialNumber + 1;

                    _db.SaveChanges();

                    return $"{staffIDNumber.StartString}{staffIDNumber.SerialNumber.ToString().PadLeft(4, '0')}";
                }
            }
        }
    }
}
