﻿using BloodHound.Domain.Const;
using Identity.API.Entities;
using Microsoft.AspNetCore.Identity;

namespace Identity.API.Data
{
    public class ApplicationDbContextSeed
    {
        public async Task SeedAsync(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            //檢查Function
            checkFunction(context);
            //檢查Admin User資料
            checkAdminUser(userManager, roleManager);
            //檢查Admin User Function資料
            checkAdminFunction(context, roleManager);
        }

        /// <summary>
        /// 檢查Function
        /// </summary>
        private void checkFunction(ApplicationDbContext db)
        {
            var operationStr = new List<string>()
            {
                "新增",
                "修改",
                "刪除",
                "檢視",
                "下載",
            };
            var functionStr = new Dictionary<FunctionGroup, List<FunctionTemp>>
            {
                {
                    new FunctionGroup { Name = "系統管理", Seq = 1, IconClass = "fas fa-fw fa-cog" },
                    new List<FunctionTemp>()
                    {
                        new FunctionTemp{
                            Name = "帳號", Controller = "account", Action = "user", Seq = 1,
                        } ,
                        new FunctionTemp{
                            Name = "角色", Controller = "account", Action = "role,roleLimit", Seq = 2,
                        } ,
                    }
                },
                {
                    new FunctionGroup { Name = "打卡管理", Seq = 2, IconClass = "fas fa-fw fa-wrench" },
                    new List<FunctionTemp>()
                    {
                        new FunctionTemp{
                            Name = "清單", Controller = "punch", Action = "list", Seq = 1,
                        } ,
                    }
                },
            };

            var groupFun = new List<FunctionGroupOperation>();
            foreach (var g in functionStr)
            {
                foreach (var f in g.Value)
                {
                    foreach (var o in operationStr)
                    {
                        groupFun.Add(new FunctionGroupOperation
                        {
                            GroupName = g.Key.Name,
                            FunctionName = f.Name,
                            Operation = o
                        });
                    }
                }
            }

            var operations = db.Operation.ToList();
            var functionGroups = db.FunctionGroup.ToList();
            var functions = db.Function.ToList();
            var functionOperations = db.FunctionOperation.ToList();
            var currentFun = new List<FunctionGroupOperation>();
            foreach (var g in functionGroups)
            {
                foreach (var f in functions)
                {
                    foreach (var o in operations)
                    {
                        currentFun.Add(new FunctionGroupOperation
                        {
                            GroupName = g.Name,
                            FunctionName = f.Name,
                            Operation = o.Name
                        });
                    }
                }
            }

            var needInsert = groupFun.Except(currentFun, new FunctionGroupOperationComparer()).ToList();
            foreach (var data in needInsert)
            {
                var operation = operations.FirstOrDefault(a => a.Name == data.Operation);
                if (operation == null)
                {
                    InsertOperation(db, data.Operation);
                    operations = db.Operation.ToList();
                    operation = operations.First(a => a.Name == data.Operation);
                }
                var functionGroup = functionGroups.FirstOrDefault(a => a.Name == data.GroupName);
                if (functionGroup == null)
                {
                    var temp = functionStr.First(a => a.Key.Name == data.GroupName);
                    InsertFunctionGroup(db, temp.Key);
                    functionGroups = db.FunctionGroup.ToList();
                    functionGroup = functionGroups.First(a => a.Name == data.GroupName);
                }
                var function = functions.FirstOrDefault(a => a.FunctionGroupId == functionGroup.FunctionGroupId && a.Name == data.FunctionName);
                if (function == null)
                {
                    var tempG = functionStr.First(a => a.Key.Name == data.GroupName);
                    var tempF = tempG.Value.First(a => a.Name == data.FunctionName);
                    InsertFunction(db, tempF, functionGroup.FunctionGroupId);
                    functions = db.Function.ToList();
                    function = functions.First(a => a.FunctionGroupId == functionGroup.FunctionGroupId && a.Name == data.FunctionName);
                }
                if (!functionOperations.Where(a =>
                    a.OperationId == operation.OperationId &&
                    a.FunctionId == function.FunctionId).Any())
                {
                    InsertFunctionOperation(db, operation.OperationId, function.FunctionId);
                }
            }
        }

        private void InsertOperation(ApplicationDbContext db, string name)
        {
            db.Operation.Add(new Operation { Name = name });
            db.SaveChanges();
        }

        private void InsertFunctionGroup(ApplicationDbContext db, FunctionGroup data)
        {
            db.FunctionGroup.Add(data);
            db.SaveChanges();
        }

        private void InsertFunction(ApplicationDbContext db, FunctionTemp data, int groupId)
        {
            db.Function.Add(new Function
            {
                Name = data.Name,
                Controller = data.Controller,
                Action = data.Action,
                IsEnabled = true,
                FunctionGroupId = groupId,
                Seq = data.Seq
            });
            db.SaveChanges();
        }

        private void InsertFunctionOperation(ApplicationDbContext db, int operationId, int functionId)
        {
            db.FunctionOperation.Add(new FunctionOperation { OperationId = operationId, FunctionId = functionId });
            db.SaveChanges();
        }

        /// <summary>
        /// 檢查Admin User資料
        /// </summary>
        private void checkAdminUser(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            var ilinkUser = userManager.FindByNameAsync(AdminUserConsts.Name).Result;
            if (ilinkUser == null)
            {
                var user = new ApplicationUser()
                {
                    UserName = AdminUserConsts.Name,
                    StaffID = AdminUserConsts.Name,
                    Email = $"{AdminUserConsts.Name}@google.com",
                    Name = AdminUserConsts.Name,
                    EnglishName = AdminUserConsts.Name,
                    IsEnabled = true
                };
                var result = userManager.CreateAsync(user, AdminUserConsts.Password).Result;
                if (result.Succeeded)
                {
                    var role = roleManager.FindByNameAsync(AdminUserConsts.RoleName).Result;
                    if (role == null)
                    {
                        var tmpRole = new ApplicationRole()
                        {
                            Name = AdminUserConsts.RoleName,
                            IsEnabled = true
                        };
                        var roleResult = roleManager.CreateAsync(tmpRole).Result;
                        if (roleResult.Succeeded)
                        {
                            _ = userManager.AddToRoleAsync(user, tmpRole.Name).Result;
                        }
                    }
                    else
                        _ = userManager.AddToRoleAsync(user, role.Name).Result;
                }
            }
        }

        /// <summary>
        /// 檢查Admin User Function資料
        /// </summary>
        private void checkAdminFunction(ApplicationDbContext db, RoleManager<ApplicationRole> roleManager)
        {
            var functionOperations = db.FunctionOperation.ToList();
            var role = roleManager.FindByNameAsync(AdminUserConsts.RoleName).Result;
            var rolePermissions = db.RolePermission.Where(a => a.RoleId == role.Id).ToList();
            var initRolePer = getInit();

            var insertEntity = initRolePer.Except(rolePermissions, new RolePermissionComparer()).ToList();
            var deleteEntity = rolePermissions.Except(initRolePer, new RolePermissionComparer()).ToList();

            db.RolePermission.RemoveRange(deleteEntity);
            db.RolePermission.AddRange(insertEntity);

            db.SaveChanges();

            List<RolePermission> getInit()
            {
                var result = new List<RolePermission>();
                foreach (var data in functionOperations)
                {
                    result.Add(new RolePermission { RoleId = role.Id, FunctionOperationId = data.FunctionOperationId, OpIsEnable = true });
                }
                return result;
            }
        }
    }

    public class FunctionGroupOperation
    {
        public string GroupName { get; set; }
        public string FunctionName { get; set; }
        public string Operation { get; set; }
    }

    public class FunctionTemp
    {
        public string Name { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int Seq { get; set; }
    }

    public class FunctionGroupOperationComparer : IEqualityComparer<FunctionGroupOperation>
    {
        public bool Equals(FunctionGroupOperation x, FunctionGroupOperation y)
        {
            return x.GroupName == y.GroupName && x.FunctionName == y.FunctionName && x.Operation == y.Operation;
        }

        public int GetHashCode(FunctionGroupOperation bx)
        {
            return $"{bx.GroupName}_{bx.FunctionName}_{bx.Operation}".GetHashCode();
        }
    }

    public class RolePermissionComparer : IEqualityComparer<RolePermission>
    {
        public bool Equals(RolePermission x, RolePermission y)
        {
            return x.RoleId == y.RoleId && x.FunctionOperationId == y.FunctionOperationId;
        }

        public int GetHashCode(RolePermission bx)
        {
            return $"{bx.RoleId}_{bx.FunctionOperationId}".GetHashCode();
        }
    }
}
