﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class FunctionGroupConfiguration : IEntityTypeConfiguration<FunctionGroup>
    {
        public void Configure(EntityTypeBuilder<FunctionGroup> builder)
        {
            builder.ToTable("FunctionGroup")
                    .HasComment("功能群組");

            builder.HasKey(e => e.FunctionGroupId);

            builder.Property(e => e.FunctionGroupId)
                .ValueGeneratedOnAdd()
                .HasColumnName("FunctionGroupId")
                .IsRequired()
                .HasComment("功能群組ID");

            builder.Property(e => e.Name)
                .HasColumnName("Name")
                .HasMaxLength(30)
                .IsRequired()
                .HasComment("功能群組名稱");

            builder.Property(e => e.Seq)
                .HasColumnName("Seq")
                .IsRequired(false)
                .HasColumnType("smallint")
                .HasComment("排序");

            builder.HasMany(e => e.Functions)
                .WithOne(e => e.FunctionGroup)
                .HasForeignKey(e => e.FunctionGroupId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Property(e => e.IconClass)
                .HasColumnName("IconClass")
                .HasMaxLength(30)
                .IsRequired(false)
                .HasComment("系統功能群組icon");
        }
    }
}
