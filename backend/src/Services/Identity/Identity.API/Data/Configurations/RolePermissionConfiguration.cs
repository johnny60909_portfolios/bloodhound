﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class RolePermissionConfiguration : IEntityTypeConfiguration<RolePermission>
    {
        public void Configure(EntityTypeBuilder<RolePermission> builder)
        {
            builder.ToTable("RolePermission")
                    .HasComment("角色功能操作授權");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .HasColumnName("Id")
                .IsRequired()
                .HasComment("角色授權Id");

            builder.Property(e => e.RoleId)
                .HasColumnName("RoleId")
                .IsRequired()
                .HasComment("角色Id");

            builder.Property(e => e.FunctionOperationId)
                .HasColumnName("FunctionOperationId")
                .IsRequired()
                .HasComment("系統功能對應操作Id");

            builder.Property(e => e.OpIsEnable)
                .HasColumnName("OpIsEnable")
                .IsRequired()
                .HasComment("程式功能是否可用");

            builder.HasOne(e => e.FunctionOperation)
                .WithMany()
                .HasForeignKey(e => e.FunctionOperationId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
