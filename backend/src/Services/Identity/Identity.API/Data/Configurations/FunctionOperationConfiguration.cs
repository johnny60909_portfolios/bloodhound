﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class FunctionOperationConfiguration : IEntityTypeConfiguration<FunctionOperation>
    {
        public void Configure(EntityTypeBuilder<FunctionOperation> builder)
        {
            builder.ToTable("FunctionOperation")
                    .HasComment("系統功能與對應操作");

            builder.HasKey(e => e.FunctionOperationId);

            builder.Property(e => e.FunctionOperationId)
                .ValueGeneratedOnAdd()
                .HasColumnName("FunctionOperationId")
                .IsRequired()
                .HasComment("系統功能對應操作Id");

            builder.Property(e => e.FunctionId)
                .HasColumnName("FunctionId")
                .IsRequired()
                .HasComment("系統功能Id");

            builder.Property(e => e.OperationId)
                .HasColumnName("OperationId")
                .IsRequired()
                .HasComment("操作種類Id");
        }
    }
}

