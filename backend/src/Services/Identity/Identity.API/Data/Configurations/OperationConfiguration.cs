﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class OperationConfiguration : IEntityTypeConfiguration<Operation>
    {
        public void Configure(EntityTypeBuilder<Operation> builder)
        {
            builder.ToTable("Operation")
                    .HasComment("操作種類");

            builder.HasKey(e => e.OperationId);

            builder.Property(e => e.OperationId)
                .ValueGeneratedOnAdd()
                .HasColumnName("OperationId")
                .IsRequired()
                .HasComment("操作種類Id");

            builder.Property(e => e.Name)
                .HasMaxLength(30)
                .HasColumnName("Name")
                .IsRequired()
                .HasComment("操作種類名稱");

            builder.HasMany(e => e.FunctionOperations)
                .WithOne(e => e.Operation)
                .HasForeignKey(e => e.OperationId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
