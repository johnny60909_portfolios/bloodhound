﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class FunctionConfiguration : IEntityTypeConfiguration<Function>
    {
        public void Configure(EntityTypeBuilder<Function> builder)
        {
            builder.ToTable("Function")
                .HasComment("系統功能");

            builder.HasKey(e => e.FunctionId);

            builder.Property(e => e.FunctionId)
                .ValueGeneratedOnAdd()
                .HasColumnName("FunctionId")
                .IsRequired()
                .HasComment("系統功能Id");

            builder.Property(e => e.Name)
                .HasMaxLength(50)
                .HasColumnName("Name")
                .IsRequired()
                .HasComment("系統功能名稱");

            builder.Property(e => e.Controller)
                .HasMaxLength(50)
                .HasColumnName("Controller")
                .IsRequired()
                .HasComment("控制器名稱");

            builder.Property(e => e.Action)
                .HasMaxLength(200)
                .HasColumnName("Action")
                .IsRequired()
                .HasComment("動作名稱");

            builder.Property(e => e.IsEnabled)
                .HasColumnName("IsEnabled")
                .IsRequired()
                .HasComment("是否啟用");

            builder.Property(e => e.FunctionGroupId)
                .HasColumnName("FunctionGroupId")
                .IsRequired()
                .HasComment("功能群組ID");

            builder.Property(e => e.Seq)
                .HasColumnName("Seq")
                .IsRequired(false)
                .HasColumnType("smallint")
                .HasComment("排序");

            builder.HasMany(e => e.FunctionOperations)
                .WithOne(e => e.Function)
                .HasForeignKey(e => e.FunctionId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
