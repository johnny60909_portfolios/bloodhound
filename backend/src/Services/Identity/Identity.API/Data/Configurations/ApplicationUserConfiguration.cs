﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class ApplicationUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.ToTable("AspNetUsers")
                    .HasComment("使用者");

            builder.HasIndex(e => e.StaffID).IsUnique();

            builder.Property(e => e.Id)
                .HasDefaultValueSql("NEWSEQUENTIALID()");

            builder.Property(d => d.IsEnabled)
                .HasColumnName("IsEnabled")
                .IsRequired(true)
                .HasComment("是否啟用");

            builder.Property(d => d.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired(false)
                .HasComment("姓名");

            builder.Property(d => d.EnglishName)
                .HasColumnName("EnglishName")
                .HasMaxLength(100)
                .IsRequired(false)
                .HasComment("英文姓名");

            builder.Property(d => d.Department)
                .HasColumnName("Department")
                .IsRequired(false)
                .HasComment("部門");

            builder.Property(d => d.StaffID)
                .HasColumnName("StaffID")
                .IsRequired()
                .HasMaxLength(50)
                .HasComment("員工編號");

            builder.Property(d => d.OnBoardDate)
                .HasColumnName("OnBoardDate")
                .IsRequired(false)
                .HasComment("到職日");

            builder.Property(d => d.JobTitle)
                .HasColumnName("JobTitle")
                .IsRequired(false)
                .HasMaxLength(50)
                .HasComment("職稱");

            builder.Property(d => d.Gender)
                .HasColumnName("Gender")
                .IsRequired(false)
                .HasMaxLength(10)
                .HasComment("性別(M:男 F:女)");

            builder.Property(d => d.IsMarried)
                .HasColumnName("IsMarried")
                .IsRequired(false)
                .HasComment("是否已婚");

            builder.Property(d => d.IsNationality)
                .HasColumnName("IsNationality")
                .IsRequired(false)
                .HasComment("是否本國籍");

            builder.Property(d => d.IdentityNo)
                .HasColumnName("IdentityNo")
                .IsRequired(false)
                .HasMaxLength(20)
                .HasComment("身份證字號");

            builder.Property(d => d.ResidentVisaNumber)
                .HasColumnName("ResidentVisaNumber")
                .IsRequired(false)
                .HasMaxLength(50)
                .HasComment("居留證號碼");

            builder.Property(d => d.PassportNumber)
                .HasColumnName("PassportNumber")
                .IsRequired(false)
                .HasMaxLength(50)
                .HasComment("護照號碼");

            builder.Property(d => d.Birthday)
                .HasColumnName("Birthday")
                .IsRequired(false)
                .HasComment("生日");

            builder.Property(d => d.Telephone)
                .HasColumnName("Telephone")
                .IsRequired(false)
                .HasMaxLength(50)
                .HasComment("電話");

            builder.Property(d => d.CurrentAddress)
                .HasColumnName("CurrentAddress")
                .IsRequired(false)
                .HasMaxLength(200)
                .HasComment("通訊地址/當前地址");

            builder.Property(d => d.ResidenceAddress)
                .HasColumnName("ResidenceAddress")
                .IsRequired(false)
                .HasMaxLength(200)
                .HasComment("戶籍地址");

            builder.Property(d => d.EducationLevel)
                .HasColumnName("EducationLevel")
                .IsRequired(false)
                .HasComment("教育程度");

            builder.Property(d => d.EducationLevelOther)
                .HasColumnName("EducationLevelOther")
                .IsRequired(false)
                .HasMaxLength(50)
                .HasComment("教育程度");

            builder.Property(d => d.EmergencyContactPerson)
                .HasColumnName("EmergencyContactPerson")
                .IsRequired(false)
                .HasMaxLength(20)
                .HasComment("緊急連絡人");

            builder.Property(d => d.EmergencyContactPersonRelation)
                .HasColumnName("EmergencyContactPersonRelation")
                .IsRequired(false)
                .HasMaxLength(10)
                .HasComment("緊急連絡人-關係");

            builder.Property(d => d.ContactPhone)
                .HasColumnName("ContactPhone")
                .IsRequired(false)
                .HasMaxLength(50)
                .HasComment("聯絡電話");

            builder.Property(d => d.ResignationDate)
                .HasColumnName("ResignationDate")
                .IsRequired(false)
                .HasComment("離職日期");
        }
    }
}
