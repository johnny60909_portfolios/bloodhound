﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    internal class ApplicationRoleRelationsConfiguration : IEntityTypeConfiguration<ApplicationRoleRelation>
    {
        public void Configure(EntityTypeBuilder<ApplicationRoleRelation> builder)
        {
            builder.ToTable("AspNetRoleRelations")
                    .HasComment("角色父子階關聯表");

            builder.HasKey(r => new { r.ParentId, r.ChildId });

            builder.Property(e => e.ParentId)
                .HasColumnName("ParentId")
                .IsRequired(true)
                .HasComment("父階角色ID");

            builder.Property(e => e.ChildId)
                .HasColumnName("ChildId")
                .IsRequired(true)
                .HasComment("子階角色ID");

            builder.HasOne(r => r.ParentRole)
                .WithMany(u => u.ChildRoleRelations)
                .HasForeignKey(r => r.ParentId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(r => r.ChildRole)
                .WithMany(u => u.ParentRoleRelations)
                .HasForeignKey(r => r.ChildId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
