﻿using Identity.API.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Identity.API.Data.Configurations
{
    public class StaffIDNumberConfiguration : IEntityTypeConfiguration<StaffIDNumber>
    {
        public void Configure(EntityTypeBuilder<StaffIDNumber> builder)
        {
            builder.ToTable("StaffIDNumber")
                .HasComment("員工編號號碼");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd()
                .HasColumnName("Id")
                .IsRequired()
                .HasComment("員工編號號碼Id");

            builder.Property(e => e.Department)
                .HasColumnName("Department")
                .IsRequired()
                .HasComment("部門");

            builder.Property(e => e.StartString)
                .HasColumnName("StartString")
                .HasMaxLength(20)
                .IsRequired(false)
                .HasComment("開頭字串");

            builder.Property(e => e.SerialNumber)
                .HasColumnName("SerialNumber")
                .IsRequired()
                .HasComment("最後一個流水號");
        }
    }
}
