using Identity.API.Data;
using Identity.API.Entities;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Identity;
using NLog.Web;

namespace Identity.API
{
    public class Program
    {
        public static IServiceProvider _provider { get; private set; }

        public static void Main(string[] args)
        {
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                IHost build = CreateHostBuilder(args).Build();

                logger.Debug("init Db Seed");

                _provider = build.Services;
                using var scope = _provider.CreateScope();
                var configuration = scope.ServiceProvider.GetService<IConfiguration>();
                var applicationDbContext = scope.ServiceProvider.GetService<ApplicationDbContext>();
                var configurationDbContext = scope.ServiceProvider.GetService<ConfigurationDbContext>();
                var userManager = scope.ServiceProvider.GetService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetService<RoleManager<ApplicationRole>>();
                new ApplicationDbContextSeed().SeedAsync(applicationDbContext, userManager, roleManager).Wait();
                new ConfigurationDbContextSeed().SeedAsync(configurationDbContext, configuration).Wait();

                logger.Debug("init Main");

                build.Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(LogLevel.Trace);
                })
                .UseNLog();
    }
}