﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Identity.API.Migrations
{
    public partial class Up_ApplicationUser_About : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Birthday",
                table: "AspNetUsers",
                type: "datetime2",
                nullable: true,
                comment: "生日");

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "聯絡電話");

            migrationBuilder.AddColumn<string>(
                name: "CurrentAddress",
                table: "AspNetUsers",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "通訊地址/當前地址");

            migrationBuilder.AddColumn<int>(
                name: "Department",
                table: "AspNetUsers",
                type: "int",
                nullable: true,
                comment: "部門");

            migrationBuilder.AddColumn<int>(
                name: "EducationLevel",
                table: "AspNetUsers",
                type: "int",
                nullable: true,
                comment: "教育程度");

            migrationBuilder.AddColumn<string>(
                name: "EducationLevelOther",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "教育程度");

            migrationBuilder.AddColumn<string>(
                name: "EmergencyContactPerson",
                table: "AspNetUsers",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "緊急連絡人");

            migrationBuilder.AddColumn<string>(
                name: "EmergencyContactPersonRelation",
                table: "AspNetUsers",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                comment: "緊急連絡人-關係");

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "AspNetUsers",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: true,
                comment: "性別(M:男 F:女)");

            migrationBuilder.AddColumn<string>(
                name: "IdentityNo",
                table: "AspNetUsers",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true,
                comment: "身份證字號");

            migrationBuilder.AddColumn<bool>(
                name: "IsMarried",
                table: "AspNetUsers",
                type: "bit",
                nullable: true,
                comment: "是否已婚");

            migrationBuilder.AddColumn<bool>(
                name: "IsNationality",
                table: "AspNetUsers",
                type: "bit",
                nullable: true,
                comment: "是否本國籍");

            migrationBuilder.AddColumn<string>(
                name: "JobTitle",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "職稱");

            migrationBuilder.AddColumn<DateTime>(
                name: "OnBoardDate",
                table: "AspNetUsers",
                type: "datetime2",
                nullable: true,
                comment: "到職日");

            migrationBuilder.AddColumn<string>(
                name: "PassportNumber",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "護照號碼");

            migrationBuilder.AddColumn<string>(
                name: "ResidenceAddress",
                table: "AspNetUsers",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: true,
                comment: "戶籍地址");

            migrationBuilder.AddColumn<string>(
                name: "ResidentVisaNumber",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "居留證號碼");

            migrationBuilder.AddColumn<DateTime>(
                name: "ResignationDate",
                table: "AspNetUsers",
                type: "datetime2",
                nullable: true,
                comment: "離職日期");

            migrationBuilder.AddColumn<string>(
                name: "StaffID",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "員工編號");

            migrationBuilder.AddColumn<string>(
                name: "Telephone",
                table: "AspNetUsers",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                comment: "電話");

            migrationBuilder.CreateTable(
                name: "StaffIDNumber",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false, comment: "員工編號號碼Id")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Department = table.Column<int>(type: "int", nullable: false, comment: "部門"),
                    StartString = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "開頭字串"),
                    SerialNumber = table.Column<int>(type: "int", nullable: false, comment: "最後一個流水號")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaffIDNumber", x => x.Id);
                },
                comment: "員工編號號碼");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StaffIDNumber");

            migrationBuilder.DropColumn(
                name: "Birthday",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CurrentAddress",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Department",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EducationLevel",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EducationLevelOther",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmergencyContactPerson",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EmergencyContactPersonRelation",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IdentityNo",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IsMarried",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "IsNationality",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "JobTitle",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OnBoardDate",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PassportNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ResidenceAddress",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ResidentVisaNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ResignationDate",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "StaffID",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Telephone",
                table: "AspNetUsers");
        }
    }
}
