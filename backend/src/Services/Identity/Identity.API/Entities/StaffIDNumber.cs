﻿using BloodHound.Domain.Enums;

namespace Identity.API.Entities
{
    /// <summary>
    /// 員工編號號碼
    /// </summary>
    public class StaffIDNumber
    {
        public int Id { get; set; }

        /// <summary>
        /// 部門
        /// </summary>
        public DepartmentEnum Department { get; set; }

        /// <summary>
        /// 開頭字串
        /// </summary>
        public string StartString { get; set; } = default!;

        /// <summary>
        /// 最後一個流水號
        /// </summary>
        public int SerialNumber { get; set; }
    }
}
