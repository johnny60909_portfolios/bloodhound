﻿namespace Identity.API.Entities
{
    /// <summary>
    /// 功能群組
    /// </summary>
    public class FunctionGroup
    {
        /// <summary>
        /// 功能群組ID
        /// </summary>
        public int FunctionGroupId { get; set; }

        /// <summary>
        /// 系統功能群組名稱
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual int? Seq { get; set; }

        /// <summary>
        /// 對應系統功能集合
        /// </summary>
        public virtual ICollection<Function> Functions { get; set; }

        /// <summary>
        /// 系統功能群組icon
        /// </summary>
        public virtual string IconClass { get; set; }
    }

}
