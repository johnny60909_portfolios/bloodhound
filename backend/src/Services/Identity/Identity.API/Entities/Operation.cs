﻿namespace Identity.API.Entities
{
    /// <summary>
    /// 操作種類
    /// </summary>
    public class Operation
    {
        /// <summary>
        /// 操作種類 Id
        /// </summary>
        public virtual int OperationId { get; set; }

        /// <summary>
        /// 操作種類名稱
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 系統功能與對應操作集合
        /// </summary>
        public virtual ICollection<FunctionOperation> FunctionOperations { get; set; }
    }
}
