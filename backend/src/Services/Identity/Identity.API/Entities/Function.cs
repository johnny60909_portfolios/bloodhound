﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Identity.API.Entities
{
    /// <summary>
    /// 系統功能
    /// </summary>
    public class Function
    {
        /// <summary>
        /// 系統功能Id
        /// </summary>
        public virtual int FunctionId { get; set; }

        /// <summary>
        /// 系統功能名稱
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 控制器名稱
        /// </summary>
        public virtual string Controller { get; set; }

        /// <summary>
        /// 動作名稱
        /// </summary>
        public virtual string Action { get; set; }

        /// <summary>
        /// 動作名稱
        /// </summary>
        [NotMapped]
        public List<string> ActionLst
        {
            get
            {
                if (Action != string.Empty)
                {
                    var actionArray = Action.Split(',');

                    return actionArray.ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        /// <summary>
        /// 是否啟用
        /// </summary>
        public virtual bool IsEnabled { get; set; } = false;

        /// <summary>
        /// 功能群組
        /// </summary>
        public virtual int? FunctionGroupId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual int? Seq { get; set; }

        /// <summary>
        /// 功能群組
        /// </summary>
        public virtual FunctionGroup FunctionGroup { get; set; }

        /// <summary>
        /// 系統功能與對應操作集合
        /// </summary>
        public virtual ICollection<FunctionOperation> FunctionOperations { get; set; }
    }

}
