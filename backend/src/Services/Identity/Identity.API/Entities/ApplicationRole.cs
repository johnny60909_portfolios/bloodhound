﻿using Microsoft.AspNetCore.Identity;

namespace Identity.API.Entities
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        public List<ApplicationUserRole>? UserRoles { get; set; }
        public List<RolePermission>? RolePermissions { get; set; }
        public List<ApplicationRoleRelation>? ParentRoleRelations { get; set; }
        public List<ApplicationRoleRelation>? ChildRoleRelations { get; set; }
        public bool IsEnabled { get; set; } = false;
    }
}


