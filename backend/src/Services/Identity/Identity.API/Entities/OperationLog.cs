﻿using BloodHound.Domain;

namespace Identity.API.Entities
{
    public class OperationLog : AuditableEntity
    {
        /// <summary>
        /// 操作記錄Id
        /// </summary>
        public Guid OperationLogId { get; set; }

        /// <summary>
        /// 功能名稱
        /// </summary>
        public string FunctionName { get; set; }

        /// <summary>
        /// 操作類型
        /// </summary>
        public int ActionTypeNo { get; set; }

        /// <summary>
        /// 說明
        /// </summary>
        public string ContentText { get; set; }
    }
}
