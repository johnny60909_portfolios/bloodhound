﻿namespace Identity.API.Entities
{
    /// <summary>
    /// 系統功能與對應操作
    /// </summary>
    public class FunctionOperation
    {
        /// <summary>
        /// 系統功能對應操作 Id
        /// </summary>
        public virtual int FunctionOperationId { get; set; }

        /// <summary>
        /// 系統功能 Id
        /// </summary>
        public virtual int FunctionId { get; set; }

        /// <summary>
        /// 對應系統功能
        /// </summary>
        public virtual Function Function { get; set; }

        /// <summary>
        /// 操作種類 Id
        /// </summary>
        public virtual int OperationId { get; set; }

        /// <summary>
        /// 對應操作種類
        /// </summary>
        public virtual Operation Operation { get; set; }
    }
}
