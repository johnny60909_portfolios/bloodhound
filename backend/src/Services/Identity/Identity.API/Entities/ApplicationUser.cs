﻿using BloodHound.Domain.Enums;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace Identity.API.Entities
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public List<ApplicationUserRole>? UserRoles { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; } = default!;

        /// <summary>
        /// 英文姓名
        /// </summary>
        public string EnglishName { get; set; } = default!;

        /// <summary>
        /// 部門
        /// </summary>
        public DepartmentEnum? Department { get; set; } = default!;

        /// <summary>
        /// 員工編號
        /// </summary>
        public string StaffID { get; set; } = default!;

        /// <summary>
        /// 到職日
        /// </summary>
        public DateTime? OnBoardDate { get; set; } = default!;

        /// <summary>
        /// 職稱
        /// </summary>
        public string JobTitle { get; set; } = default!;

        /// <summary>
        /// 性別(M:男 F:女)
        /// </summary>
        public string Gender { get; set; } = default!;

        /// <summary>
        /// 是否已婚
        /// </summary>
        public bool? IsMarried { get; set; } = default!;

        /// <summary>
        /// 是否本國籍
        /// </summary>
        public bool? IsNationality { get; set; } = default!;

        /// <summary>
        /// 身份證字號
        /// </summary>
        public string IdentityNo { get; set; } = default!;

        /// <summary>
        /// 居留證號碼
        /// </summary>
        public string ResidentVisaNumber { get; set; } = default!;

        /// <summary>
        /// 護照號碼
        /// </summary>
        public string PassportNumber { get; set; } = default!;

        /// <summary>
        /// 生日
        /// </summary>
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        public string Telephone { get; set; } = default!;

        /// <summary>
        /// 通訊地址/當前地址
        /// </summary>
        public string CurrentAddress { get; set; } = default!;

        /// <summary>
        /// 戶籍地址
        /// </summary>
        public string ResidenceAddress { get; set; } = default!;

        /// <summary>
        /// 教育程度
        /// </summary>
        public EducationLevelEnum? EducationLevel { get; set; } = default!;

        /// <summary>
        /// 教育程度 - 其他
        /// </summary>
        public string EducationLevelOther { get; set; } = default!;

        /// <summary>
        /// 緊急連絡人
        /// </summary>
        public string EmergencyContactPerson { get; set; } = default!;

        /// <summary>
        /// 緊急連絡人-關係
        /// </summary>
        public string EmergencyContactPersonRelation { get; set; } = default!;

        /// <summary>
        /// 聯絡電話
        /// </summary>
        public string ContactPhone { get; set; } = default!;

        /// <summary>
        /// 離職日期
        /// </summary>
        public DateTime? ResignationDate { get; set; } = default!;

        /// <summary>
        /// 是否啟用
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// 年齡
        /// </summary>
        [NotMapped]
        public int? Age
        {
            get
            {
                if (Birthday.HasValue)
                {
                    var today = DateTime.Today;
                    var age = today.Year - Birthday.Value.Year;
                    if (Birthday.Value.Date > today.AddYears(-age))
                        age--;
                    return age;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
