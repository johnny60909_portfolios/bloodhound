﻿namespace Identity.API.Entities
{
    /// <summary>
    /// 角色功能操作授權
    /// </summary>
    public class RolePermission
    {
        /// <summary>
        /// 角色授權 Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 角色 Id
        /// </summary>
        public Guid RoleId { get; set; }

        public ApplicationRole? Role { get; set; }

        /// <summary>
        /// 系統功能對應操作 Id
        /// </summary>
        public int FunctionOperationId { get; set; }

        public FunctionOperation? FunctionOperation { get; set; }

        /// <summary>
        /// 程式功能是否可用
        /// </summary>
        public bool OpIsEnable { get; set; }
    }
}
