﻿namespace Identity.API.Entities
{
    public class ApplicationRoleRelation
    {
        /// <summary>
        /// 父階角色
        /// </summary>
        public Guid ParentId { get; set; }
        /// <summary>
        /// 子階角色
        /// </summary>
        public Guid ChildId { get; set; }
        public ApplicationRole? ParentRole { get; set; }
        public ApplicationRole? ChildRole { get; set; }
    }
}

