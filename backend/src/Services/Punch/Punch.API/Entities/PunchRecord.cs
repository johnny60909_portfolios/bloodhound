﻿namespace Punch.API.Entities
{
    /// <summary>
    /// 打卡紀錄
    /// </summary>
    public class PunchRecord
    {
        /// <summary>
        /// 打卡紀錄 Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 打卡時間
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// 打卡人員 Id
        /// </summary>
        public Guid UserId { get; set; }
    }
}
