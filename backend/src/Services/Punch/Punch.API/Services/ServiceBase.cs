﻿using Punch.API.Data;

namespace Punch.API.Services
{
    public class ServiceBase
    {
        protected readonly ILogger<ServiceBase> _logger;
        protected readonly ApplicationDbContext _db;

        public ServiceBase(ILogger<ServiceBase> logger, ApplicationDbContext db)
        {
            _logger = logger;
            _db = db;
        }
    }
}
