﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Punch;
using Microsoft.EntityFrameworkCore;
using Punch.API.Data;
using Punch.API.Entities;
using Punch.API.Services.Interface;
using static BloodHound.Domain.Dto.Identity.UserDto;

namespace Punch.API.Services
{
    public class PunchService : ServiceBase, IPunchService
    {
        public PunchService(
            ILogger<PunchService> logger,
            ApplicationDbContext db) : base(logger, db)
        {

        }

        public Task<List<PunchDto.Punch>> GetPunchAsync(PageDto pageSetting, Guid UserId)
        {
            var query = _db.PunchRecord.AsNoTracking()
                .Where(a => a.UserId == UserId)
                .AsQueryable();

            var queryTime = query.Select(a => a.Time.Date).Distinct();

            //總排序
            queryTime = queryTime.OrderByDescending(a => a);

            if (pageSetting != null)
            {
                pageSetting.TotalCount = queryTime.Count();
                if (pageSetting.Page.HasValue
                    && pageSetting.Page.Value > 0
                    && pageSetting.Rows.HasValue
                    && pageSetting.Rows.Value > 0)
                {
                    queryTime = queryTime
                        .Skip((pageSetting.Rows.Value - 1) * pageSetting.Page.Value)
                        .Take(pageSetting.Page.Value);
                }
            }

            var result = new List<PunchDto.Punch>();

            var disTime = queryTime.ToList();

            if (disTime.Count == 0)
            {
                return Task.FromResult(result);
            }

            var startTime = disTime.Min();
            var endTime = disTime.Max();

            var queryData = query.Where(a => a.Time.Date >= startTime && a.Time.Date <= endTime).ToList();

            foreach (var d in disTime)
            {
                var temp = queryData.Where(a => a.Time.Date == d).ToList();

                var data = new PunchDto.Punch();
                data.PushDate = d;
                data.FirstTime = temp.OrderBy(a => a.Time).FirstOrDefault()?.Time.TimeOfDay;
                data.LastTime = temp.OrderByDescending(a => a.Time).FirstOrDefault()?.Time.TimeOfDay;
                data.PushTimes = temp.OrderBy(a => a.Time).Select(a => a.Time.TimeOfDay).ToList();

                result.Add(data);
            }

            return Task.FromResult(result);
        }

        public async Task PunchAsync(Guid LoginUserId)
        {
            var insert = new PunchRecord()
            {
                Id = Guid.NewGuid(),
                Time = DateTime.Now,
                UserId = LoginUserId
            };

            _db.PunchRecord.Add(insert);

            await _db.SaveChangesAsync();
        }

        public Task<List<PunchDto.PunchList>> GetAllPunchAsync(PunchDto.SearchParams filter)
        {
            var query = _db.PunchRecord.AsNoTracking()
                .AsQueryable();

            if (!string.IsNullOrEmpty(filter.UserId) && Guid.TryParse(filter.UserId, out var tmp1))
            {
                query = query.Where(a => a.UserId == tmp1);
            }

            if (!string.IsNullOrEmpty(filter.StartDate) && DateTime.TryParse(filter.StartDate, out var tmp2))
            {
                query = query.Where(a => a.Time.Date >= tmp2.Date);
            }

            if (!string.IsNullOrEmpty(filter.EndDate) && DateTime.TryParse(filter.EndDate, out var tmp3))
            {
                query = query.Where(a => a.Time.Date <= tmp3.Date);
            }

            var queryGroup = query.GroupBy(a => new { a.UserId, a.Time.Date }).Select(a => new { a.Key.UserId, a.Key.Date });

            //總排序
            queryGroup = queryGroup.OrderByDescending(a => a.Date).ThenBy(a => a.UserId);

            if (filter.PageSetting != null)
            {
                filter.PageSetting.TotalCount = queryGroup.Count();
                if (filter.PageSetting.Page.HasValue
                    && filter.PageSetting.Page.Value > 0
                    && filter.PageSetting.Rows.HasValue
                    && filter.PageSetting.Rows.Value > 0)
                {
                    queryGroup = queryGroup
                        .Skip((filter.PageSetting.Rows.Value - 1) * filter.PageSetting.Page.Value)
                        .Take(filter.PageSetting.Page.Value);
                }
            }

            var result = new List<PunchDto.PunchList>();

            var disGroup = queryGroup.ToList();

            if (disGroup.Count == 0)
            {
                return Task.FromResult(result);
            }

            var startTime = disGroup.Select(a => a.Date).Min();
            var endTime = disGroup.Select(a => a.Date).Max();
            var userIds = disGroup.Select(a => a.UserId).Distinct();

            var queryData = query.Where(a => a.Time.Date >= startTime && a.Time.Date <= endTime && userIds.Contains(a.UserId)).ToList();

            foreach (var d in disGroup)
            {
                var temp = queryData.Where(a => a.Time.Date == d.Date && a.UserId == d.UserId).ToList();

                var data = new PunchDto.PunchList();
                data.UserId = d.UserId;
                data.PushDate = d.Date;
                data.FirstTime = temp.OrderBy(a => a.Time).FirstOrDefault()?.Time.TimeOfDay;
                data.LastTime = temp.OrderByDescending(a => a.Time).FirstOrDefault()?.Time.TimeOfDay;
                data.PushTimes = temp.OrderBy(a => a.Time).Select(a => a.Time.TimeOfDay).ToList();

                result.Add(data);
            }

            return Task.FromResult(result);
        }
    }
}
