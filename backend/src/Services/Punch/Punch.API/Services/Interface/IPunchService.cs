﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Punch;

namespace Punch.API.Services.Interface
{
    public interface IPunchService
    {
        Task<List<PunchDto.PunchList>> GetAllPunchAsync(PunchDto.SearchParams filter);
        Task<List<PunchDto.Punch>> GetPunchAsync(PageDto pageSetting, Guid UserId);
        Task PunchAsync(Guid LoginUserId);
    }
}
