﻿using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Punch;
using BloodHound.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Punch.API.Services.Interface;
using System.Collections.Generic;
using System.Net;
using TVGHiHMS.Core.Dto;

namespace Punch.API.Controllers.Api
{
    public class PunchController : ApiControllerBase
    {
        private readonly IPunchService _punchService;

        public PunchController(
            ILogger<PunchController> logger,
            IPunchService punchService) : base(logger)
        {
            _punchService = punchService;
        }

        /// <summary>
        /// 取得Punch清單
        /// </summary>
        [HttpPost]
        [Route("GetPunchListAsync")]
        public async Task<JsonTableResponse<List<PunchDto.Punch>>> GetPunchListAsync(PageDto pageSetting)
        {
            var response = new JsonTableResponse<List<PunchDto.Punch>>();
            response.Result = await _punchService.GetPunchAsync(pageSetting, LoginUserId);
            response.Total = pageSetting.TotalCount;
            return response;
        }

        /// <summary>
        /// Punch打卡
        /// </summary>
        [HttpPost]
        [Route("PunchAsync")]
        public async Task PunchAsync()
        {
            await _punchService.PunchAsync(LoginUserId);
        }

        /// <summary>
        /// 取得所有人的Punch清單
        /// </summary>
        [HttpPost]
        [Route("GetAllPunchAsync")]
        public async Task<JsonTableResponse<List<PunchDto.PunchList>>> GetAllPunchAsync(PunchDto.SearchParams filter)
        {
            var response = new JsonTableResponse<List<PunchDto.PunchList>>();
            response.Result = await _punchService.GetAllPunchAsync(filter);
            response.Total = filter.PageSetting.TotalCount;
            return response;
        }
    }
}
