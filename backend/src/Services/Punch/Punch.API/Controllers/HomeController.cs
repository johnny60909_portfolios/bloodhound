﻿using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace Punch.API
{    
    public class HomeController : Controller
    {
        public HomeController(
            ILogger<HomeController> logger
            )
        {
        }

        public IActionResult Index()
        {
            return new RedirectResult("~/swagger");
        }

        /// <summary>
        /// 系統404頁面
        /// </summary>
        /// <returns>畫面</returns>
        public IActionResult NotFoundError()
        {
            return BadRequest("找不到此URL");
        }       
    }
}
