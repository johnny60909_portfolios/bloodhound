﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Punch.API.Entities;

namespace Punch.API.Data.Configurations
{
    internal class PunchRecordConfiguration : IEntityTypeConfiguration<PunchRecord>
    {
        public void Configure(EntityTypeBuilder<PunchRecord> builder)
        {
            builder.ToTable("PunchRecord")
                    .HasComment("打卡紀錄");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .IsRequired()
                .HasComment("打卡紀錄Id");

            builder.Property(e => e.Time)
                .IsRequired()
                .HasComment("打卡時間");

            builder.Property(e => e.UserId)
                .IsRequired()
                .HasComment("打卡人員Id");
        }
    }
}
