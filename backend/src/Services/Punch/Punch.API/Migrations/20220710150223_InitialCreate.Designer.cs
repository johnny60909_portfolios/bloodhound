﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Punch.API.Data;

#nullable disable

namespace Punch.API.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20220710150223_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("Punch.API.Entities.PunchRecord", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier")
                        .HasComment("打卡紀錄Id");

                    b.Property<DateTime>("Time")
                        .HasColumnType("datetime2")
                        .HasComment("打卡時間");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uniqueidentifier")
                        .HasComment("打卡人員Id");

                    b.HasKey("Id");

                    b.ToTable("PunchRecord", (string)null);

                    b.HasComment("打卡紀錄");
                });
#pragma warning restore 612, 618
        }
    }
}
