﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Punch.API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PunchRecord",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "打卡紀錄Id"),
                    Time = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "打卡時間"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, comment: "打卡人員Id")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PunchRecord", x => x.Id);
                },
                comment: "打卡紀錄");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PunchRecord");
        }
    }
}
