﻿using BloodHound.Domain.Const;
using BloodHound.Infrastructure.Filters;
using BloodHound.Infrastructure.Utility;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using NSwag;
using NSwag.Generation.Processors.Security;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Polly;
using Web.BloodHound.HttpAggregator.Services;
using Web.BloodHound.HttpAggregator.Services.Interface;

namespace Web.BloodHound.HttpAggregator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .SetIsOriginAllowed((host) => true)
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<TokenCache>();

            var authenticationProviderKey = "ApiKey";
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(authenticationProviderKey, options =>
            {
                //// base-address of your identityserver
                options.Authority = Configuration["Services:Identity"];

                //// IdentityServer emits a typ header by default, recommended extra check
                options.TokenValidationParameters.ValidTypes = new[] { "at+jwt" };

                options.RequireHttpsMetadata = false;

                //// if you are using API resources, you can specify the name here
                options.Audience = Configuration["AuthServer:Scope"];
            })
            .AddJwtBearer(options =>
            {
                //// base-address of your identityserver
                options.Authority = Configuration["Services:Identity"];

                //// IdentityServer emits a typ header by default, recommended extra check
                options.TokenValidationParameters.ValidTypes = new[] { "at+jwt" };

                options.RequireHttpsMetadata = false;

                //// if you are using API resources, you can specify the name here
                options.Audience = Configuration["AuthServer:Scope"];
            });

            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            })
            //關閉FluentValidation預設驗證
            .AddFluentValidation(options => options.AutomaticValidationEnabled = false)
            //關閉預設模型驗證過濾器
            .ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            // Add OpenAPI v3 document
            services.AddOpenApiDocument(config =>
            {
                var apiScheme = new OpenApiSecurityScheme()
                {
                    Type = OpenApiSecuritySchemeType.ApiKey,
                    Name = "Authorization",
                    In = OpenApiSecurityApiKeyLocation.Header,
                    Description = "Copy this into the value field: Bearer {token}"
                };

                config.AddSecurity("JWT Token", Enumerable.Empty<string>(), apiScheme);

                config.OperationProcessors.Add(
                    new AspNetCoreOperationSecurityScopeProcessor("JWT Token"));
            });

            //使用 Ocelot、Polly
            services.AddOcelot().AddPolly();

            services.AddTransient<TokenHttpHandler>();

            #region API HttpClientFactory 註冊

            //使用 IHttpClientFactory
            services.AddHttpClient(HttpClientConsts.Identity_API_Name, c =>
            {
                c.BaseAddress = new Uri(Configuration["Services:Identity"]);
            }).ConfigurePrimaryHttpMessageHandler(h =>
            {
                var handler = new HttpClientHandler();
                //繞過無效SSL證書 錯誤
                handler.ServerCertificateCustomValidationCallback = delegate { return true; };
                return handler;
            })
            .AddPolicyHandler(HttpPolly.GetRetryPolicy())
            .AddHttpMessageHandler<TokenHttpHandler>();

            services.AddHttpClient(HttpClientConsts.Punch_API_Name, c =>
            {
                c.BaseAddress = new Uri(Configuration["Services:Punch"]);
            }).ConfigurePrimaryHttpMessageHandler(h =>
            {
                var handler = new HttpClientHandler();
                //繞過無效SSL證書 錯誤
                handler.ServerCertificateCustomValidationCallback = delegate { return true; };
                return handler;
            })
            .AddPolicyHandler(HttpPolly.GetRetryPolicy())
            .AddHttpMessageHandler<TokenHttpHandler>();

            services.AddHttpClient(HttpClientConsts.Saga_API_Name, c =>
            {
                c.BaseAddress = new Uri(Configuration["Services:Saga"]);
            }).ConfigurePrimaryHttpMessageHandler(h =>
            {
                var handler = new HttpClientHandler();
                //繞過無效SSL證書 錯誤
                handler.ServerCertificateCustomValidationCallback = delegate { return true; };
                return handler;
            })
            .AddPolicyHandler(HttpPolly.GetRetryPolicy())
            .AddHttpMessageHandler<TokenHttpHandler>();

            #endregion

            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<IPunchService, PunchService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }

            // TODO: 正式上線後要移入IsDevelopment裡
            // 註冊 Swagger
            app.UseOpenApi();
            app.UseSwaggerUi3();

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseReDoc(config =>  // serve ReDoc UI
            {
                // 這裡的 Path 用來設定 ReDoc UI 的路由 (網址路徑) (一定要以 / 斜線開頭)
                config.Path = "/redoc";
            });

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            app.UseOcelot().Wait();
        }
    }
}
