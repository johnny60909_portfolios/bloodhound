﻿using Microsoft.AspNetCore.Mvc;
using System.Text;

namespace Web.BloodHound.HttpAggregator.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(
            ILogger<HomeController> logger
            )
        {
        }

        public IActionResult Index()
        {
            return new RedirectResult("~/swagger");
        }     
    }
}
