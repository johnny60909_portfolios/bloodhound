﻿using BloodHound.Domain.Dto.Punch;
using BloodHound.Domain.Dto;
using BloodHound.Infrastructure.Utility;
using Microsoft.AspNetCore.Mvc;
using TVGHiHMS.Core.Dto;
using Web.BloodHound.HttpAggregator.Services.Interface;

namespace Web.BloodHound.HttpAggregator.Controllers.Api
{
    public class PunchController : ApiControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IPunchService _punchService;

        public PunchController(
            ILogger<PunchController> logger,
            IHttpContextAccessor httpContextAccessor,
            TokenCache tokenCache,
            IConfiguration configuration,
            IPunchService punchService) : base(logger, httpContextAccessor, tokenCache)
        {
            _configuration = configuration;
            _punchService = punchService;
        }

        /// <summary>
        /// 取得Punch清單
        /// </summary>
        [HttpPost]
        [Route("GetPunchListAsync")]
        public async Task<JsonTableResponse<List<PunchDto.PunchList>>> GetPunchListAsync(PunchDto.SearchParams filter)
        {
            var response = new JsonTableResponse<List<PunchDto.PunchList>>();
            response.Result = await _punchService.GetPunchListAsync(filter);
            response.Total = filter.PageSetting.TotalCount;
            return response;
        }
    }
}
