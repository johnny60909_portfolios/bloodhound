﻿using BloodHound.Domain.Dto.Web;
using BloodHound.Infrastructure.Utility;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.BloodHound.HttpAggregator.Services.Interface;

namespace Web.BloodHound.HttpAggregator.Controllers.Api
{
    public class AuthController : ApiControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IIdentityService _identityService;

        public AuthController(
            ILogger<AuthController> logger,
            IHttpContextAccessor httpContextAccessor,
            TokenCache tokenCache,
            IConfiguration configuration,
            IIdentityService identityService) : base(logger, httpContextAccessor, tokenCache)
        {
            _configuration = configuration;
            _identityService = identityService;
        }

        /// <summary>
        /// 帳號登入
        /// </summary>
        /// <param name="login">登入帳號密碼</param>
        /// <returns>JWTocken</returns>
        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<AccountDto.LoginResult> Login(AccountDto.Login data)
        {
            var result = new AccountDto.LoginResult();

            try
            {
                var host = _configuration["Services:Identity"];
                var clientId = _configuration["AuthServer:ClientId"];
                var secret = _configuration["AuthServer:ClientSecret"];
                var scope = _configuration["AuthServer:Scope"];

                var userData = await _identityService.GetLoginUserAsync(data.Account);
                if (userData == null)
                {
                    throw new Exception("找不到此帳號");
                }

                using var client = new HttpClient();

                var response = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
                {
                    Address = host + "/connect/token",
                    GrantType = "password",
                    ClientId = clientId,
                    ClientSecret = secret,
                    Scope = scope,
                    UserName = userData.Account,
                    Password = data.Password,
                });

                if (response.IsError)
                {
                    if (!string.IsNullOrEmpty(response.Error))
                        throw new Exception(response.Error);
                    if (!string.IsNullOrEmpty(response.ErrorDescription))
                        throw new Exception(response.ErrorDescription);
                    throw new Exception("token取得失敗");
                }

                result.Token = response.AccessToken;
                result.ExpiresIn = DateTime.Now.AddSeconds(response.ExpiresIn);
                result.Succeeded = true;
                result.User = userData;
            }
            catch (Exception ex)
            {
                _logger.LogError("登入失敗：" + ex.Message);
                result.Message = ex.Message;
            }

            return result;
        }
    }
}
