﻿using BloodHound.Infrastructure.Utility;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Web.BloodHound.HttpAggregator.Controllers.Api
{
    [Authorize]
    [Route("api/aggregation/[controller]")]
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {
        protected readonly ILogger<ApiControllerBase> _logger;
        protected readonly IHttpContextAccessor _httpContextAccessor;
        protected readonly TokenCache _tokenCache;

        protected ApiControllerBase(
            ILogger<ApiControllerBase> logger,
            IHttpContextAccessor httpContextAccessor,
            TokenCache tokenCache)
        {
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _tokenCache = tokenCache;

            var authorizationHeader = _httpContextAccessor?.HttpContext?.Request?.Headers["Authorization"] ?? string.Empty;
            var accessToken = authorizationHeader.ToString().Replace(JwtBearerDefaults.AuthenticationScheme, "");
            _tokenCache.SetAccessToken(accessToken);
        }
    }
}
