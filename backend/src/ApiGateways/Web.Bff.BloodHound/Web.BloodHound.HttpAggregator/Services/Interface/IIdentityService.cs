﻿using BloodHound.Domain.Dto.Identity;

namespace Web.BloodHound.HttpAggregator.Services.Interface
{
    public interface IIdentityService
    {
        Task<UserDto.LoginUser?> GetLoginUserAsync(string staffID);
    }
}
