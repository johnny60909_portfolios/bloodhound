﻿using BloodHound.Domain.Dto.Punch;

namespace Web.BloodHound.HttpAggregator.Services.Interface
{
    public interface IPunchService
    {
        Task<List<PunchDto.PunchList>> GetPunchListAsync(PunchDto.SearchParams filter);
    }
}
