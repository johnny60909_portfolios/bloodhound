﻿using BloodHound.Domain.Const;
using BloodHound.Domain.Dto;
using BloodHound.Domain.Dto.Identity;
using BloodHound.Domain.Dto.Punch;
using BloodHound.Infrastructure.Exceptions;
using TVGHiHMS.Core.Dto;
using Web.BloodHound.HttpAggregator.Services.Interface;

namespace Web.BloodHound.HttpAggregator.Services
{
    public class PunchService : ServiceBase, IPunchService
    {
        public PunchService(
            ILogger<PunchService> logge,
            IHttpClientFactory httpClientFactory) : base(logge, httpClientFactory)
        {

        }

        public async Task<List<PunchDto.PunchList>> GetPunchListAsync(PunchDto.SearchParams filter)
        {
            var userKeyValue = await GetAPIResult<List<KeyValueDto<string, string>>>(HttpClientConsts.Identity_API_Name, "api/User/GetAllUserKeyValueAsync");
            var punchResult = await PostAPIResult<PunchDto.SearchParams, JsonTableResponse<List<PunchDto.PunchList>>>(HttpClientConsts.Punch_API_Name, "api/Punch/GetAllPunchAsync", filter);

            if (userKeyValue == null || punchResult?.Result == null)
                throw new DomainException("查詢失敗");

            filter.PageSetting.TotalCount = punchResult.Total;

            foreach (var data in punchResult.Result)
            {
                data.UserName = userKeyValue.FirstOrDefault(a => Guid.Parse(a.Key) == data.UserId)?.Value ?? string.Empty;
            }

            return punchResult.Result;
        }
    }
}
