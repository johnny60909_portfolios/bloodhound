﻿using IdentityServer4.Models;
using Microsoft.AspNetCore.Http;
using System.Text.Json;

namespace Web.BloodHound.HttpAggregator.Services
{
    public class ServiceBase
    {
        private readonly ILogger<ServiceBase> _logger;
        protected readonly IHttpClientFactory _httpClientFactory;
        const string scheduleName = "Service API => ";

        public ServiceBase(
            ILogger<ServiceBase> logger,
            IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        protected async Task<T?> GetAPIResult<T>(string apiName, string url) where T : class
        {
            var httpClient = _httpClientFactory.CreateClient(apiName);
            try
            {
                using var response = await httpClient.GetAsync(url);
                var message = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    var statusCode = response?.StatusCode != null ? (int)response.StatusCode : 0;
                    _logger.LogError($"{scheduleName}{apiName} => {response?.RequestMessage?.RequestUri} Request失敗, StatusCode：{statusCode}, Msg：{message}");
                }
                else
                {
                    var options = new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    };
                    var result = JsonSerializer.Deserialize<T>(message, options);
                    return result ?? null;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{scheduleName}{apiName} => {url} 發生例外狀況, Msg：{e}");
            }

            return null;
        }

        protected async Task<T2?> PostAPIResult<T1, T2>(string apiName, string url, T1 objectData) where T1 : class where T2 : class
        {
            var httpClient = _httpClientFactory.CreateClient(apiName);
            try
            {
                using var response = await httpClient.PostAsJsonAsync(url, objectData);
                var message = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    var statusCode = response?.StatusCode != null ? (int)response.StatusCode : 0;
                    _logger.LogError($"{scheduleName}{apiName} => {response?.RequestMessage?.RequestUri} Request失敗, StatusCode：{statusCode}, Msg：{message}");
                }
                else
                {
                    var options = new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    };
                    var result = JsonSerializer.Deserialize<T2>(message, options);
                    return result ?? null;
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{scheduleName}{apiName} => {url} 發生例外狀況, Msg：{e}");
            }

            return null;
        }
    }
}
