﻿using BloodHound.Domain.Const;
using BloodHound.Domain.Dto.Identity;
using BloodHound.Infrastructure.Extension;
using Web.BloodHound.HttpAggregator.Services.Interface;

namespace Web.BloodHound.HttpAggregator.Services
{
    public class IdentityService : ServiceBase, IIdentityService
    {
        public IdentityService(
            ILogger<IdentityService> logge,
            IHttpClientFactory httpClientFactory) : base(logge, httpClientFactory)
        {

        }

        public async Task<UserDto.LoginUser?> GetLoginUserAsync(string staffID)
        {
            var result = await GetAPIResult<UserDto.LoginUser>(HttpClientConsts.Identity_API_Name, "api/User/GetLoginUserAsync?staffID=" + staffID);
            return result;
        }
    }
}
