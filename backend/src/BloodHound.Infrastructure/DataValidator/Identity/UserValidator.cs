﻿using BloodHound.Domain.Const;
using BloodHound.Domain.Enums;
using BloodHound.Infrastructure.Exceptions;
using FluentValidation;
using static BloodHound.Domain.Dto.Identity.UserDto;

namespace BloodHound.Infrastructure.DataValidator.Identity
{
    public class UserValidator
    {
        public class CreateUserValidator : AbstractValidator<EditUser>
        {
            public CreateUserValidator()
            {
                RuleFor(x => x.RoleId)
                    .NotEmpty()
                    .WithName("角色")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.Name)
                    .NotEmpty()
                    .WithName("姓名")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.Password)
                    .NotEmpty()
                    .WithName("密碼")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.PasswordCheck)
                    .NotEmpty()
                    .WithName("密碼確認")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.Department)
                    .NotEmpty()
                    .WithName("部門")
                    .WithMessage("{PropertyName}不可為空");

                When(x => !string.IsNullOrEmpty(x.Password) && !string.IsNullOrEmpty(x.PasswordCheck), () =>
                {
                    RuleFor(x => x)
                        .Must(x => x.Password == x.PasswordCheck)
                        .WithName("密碼、密碼確認")
                        .WithMessage("{PropertyName}不一致");
                });

                When(x => !string.IsNullOrEmpty(x.Department), () =>
                {
                    RuleFor(x => x)
                        .Must(x =>
                            Enum.TryParse(x.Department, true, out DepartmentEnum tmp1) && Enum.IsDefined(typeof(DepartmentEnum), tmp1) &&
                            int.TryParse(x.Department, out var tmp2))
                        .WithName("部門")
                        .WithMessage("{PropertyName}格式錯誤");
                });

                When(x => !string.IsNullOrEmpty(x.EducationLevel), () =>
                {
                    RuleFor(x => x)
                        .Must(x =>
                            Enum.TryParse(x.EducationLevel, true, out EducationLevelEnum tmp1) && Enum.IsDefined(typeof(EducationLevelEnum), tmp1) &&
                            int.TryParse(x.EducationLevel, out var tmp2))
                        .WithName("教育程度")
                        .WithMessage("{PropertyName}格式錯誤");
                });
            }
        }

        public class EditUserListValidator : AbstractValidator<EditUser>
        {
            public EditUserListValidator()
            {
                RuleFor(x => x.RoleId)
                    .NotEmpty()
                    .WithName("角色")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.Name)
                    .NotEmpty()
                    .WithName("姓名")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x)
                    .Must(x => x.Account == AdminUserConsts.Name || !string.IsNullOrEmpty(x.Department))
                    .WithName("部門")
                    .WithMessage("{PropertyName}不可為空");

                When(x => !string.IsNullOrEmpty(x.Department), () =>
                {
                    RuleFor(x => x)
                        .Must(x =>
                            Enum.TryParse(x.Department, true, out DepartmentEnum tmp1) && Enum.IsDefined(typeof(DepartmentEnum), tmp1) &&
                            int.TryParse(x.Department, out var tmp2))
                        .WithName("部門")
                        .WithMessage("{PropertyName}格式錯誤");
                });

                When(x => !string.IsNullOrEmpty(x.EducationLevel), () =>
                {
                    RuleFor(x => x)
                        .Must(x =>
                            Enum.TryParse(x.EducationLevel, true, out EducationLevelEnum tmp1) && Enum.IsDefined(typeof(EducationLevelEnum), tmp1) &&
                            int.TryParse(x.EducationLevel, out var tmp2))
                        .WithName("教育程度")
                        .WithMessage("{PropertyName}格式錯誤");
                });
            }
        }

        public class UpdatePasswordValidator : AbstractValidator<UpdatePassword>
        {
            public UpdatePasswordValidator()
            {
                RuleFor(x => x.Password)
                    .NotEmpty()
                    .WithName("密碼")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.PasswordCheck)
                    .NotEmpty()
                    .WithName("密碼確認")
                    .WithMessage("{PropertyName}不可為空");

                When(x => !string.IsNullOrEmpty(x.Password) && !string.IsNullOrEmpty(x.PasswordCheck), () =>
                {
                    RuleFor(x => x)
                        .Must(x => x.Password == x.PasswordCheck)
                        .WithName("密碼、密碼確認")
                        .WithMessage("{PropertyName}不一致");
                });
            }
        }
    }
}
