﻿using FluentValidation;
using static BloodHound.Domain.Dto.Identity.RoleDto;

namespace BloodHound.Infrastructure.DataValidator.Identity
{
    public class RoleValidator
    {
        public class CreateRoleValidator : AbstractValidator<CreateRole>
        {
            public CreateRoleValidator()
            {
                RuleFor(x => x.Name)
                    .NotEmpty()
                    .WithName("角色名稱")
                    .WithMessage("{PropertyName}不可為空");

                RuleFor(x => x.ParentRoleId)
                    .NotEmpty()
                    .WithName("父階角色")
                    .WithMessage("{PropertyName}不可為空");

            }
        }
    }
}
