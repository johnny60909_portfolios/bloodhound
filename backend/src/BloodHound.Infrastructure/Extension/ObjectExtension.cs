﻿using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace BloodHound.Infrastructure.Extension;

public static class ObjectExtension
{
    /// <summary>
    /// 轉成Json格式字串
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string ConvertJsonSerialize(this object data)
    {
        var options = new JsonSerializerOptions
        {
            Encoder = JavaScriptEncoder.Create(UnicodeRanges.All)
        };

        return JsonSerializer.Serialize(data, options);
    }

    /// <summary>
    /// Json String轉成物件
    /// </summary>
    /// <returns></returns>
    public static T? DeserializeJsonString<T>(this string jsonStr) where T : class
    {
        var options = new JsonSerializerOptions
        {
            Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
            PropertyNameCaseInsensitive = true,
        };

        if (string.IsNullOrEmpty(jsonStr))
            return default;

        return JsonSerializer.Deserialize<T>(jsonStr, options);
    }
}
