﻿namespace BloodHound.Infrastructure.Utility
{
    public class TokenCache
    {
        private static string AccessToken { get; set; } = default!;

        public void SetAccessToken(string token)
        {
            AccessToken = token;
        }

        public string GetAccessToken()
        {
            return AccessToken;
        }
    }
}
