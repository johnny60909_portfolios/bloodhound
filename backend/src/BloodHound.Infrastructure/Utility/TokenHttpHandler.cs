﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net.Http.Headers;

namespace BloodHound.Infrastructure.Utility
{
    public class TokenHttpHandler : DelegatingHandler
    {
        private readonly TokenCache _tokenCache;
        public TokenHttpHandler(TokenCache tokenCache)
        {
            _tokenCache = tokenCache;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // Add Authorization header.
            if (!string.IsNullOrEmpty(_tokenCache.GetAccessToken()))
                request.Headers.Authorization = new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, _tokenCache.GetAccessToken());

            return base.SendAsync(request, cancellationToken);
        }
    }
}
