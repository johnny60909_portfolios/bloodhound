﻿using Polly;
using Polly.Extensions.Http;

namespace BloodHound.Infrastructure.Utility
{
    public static class HttpPolly
    {
        /// <summary>
        /// HttpClient 重試設定
        /// </summary>
        /// <returns></returns>
        public static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy(int retrySeconds = 1)
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()//遇到 HTTP 5xx
                .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)//或是得到 404 NoFound
                .WaitAndRetryAsync(3,
                retryAttempt => TimeSpan.FromSeconds(retrySeconds)//重試3次，間隔秒數為1
                , (result, timeSpan, retryCount, context) =>
                {
                    var logger = NLog.LogManager.GetCurrentClassLogger();
                    if (result?.Result?.StatusCode != null)
                    {
                        var statusCode = (int)result?.Result?.StatusCode;
                        var requestUri = result?.Result?.RequestMessage?.RequestUri;
                        logger.Error($"Request API => 請求失敗, StatusCode：{statusCode}, 重試次數：{retryCount}, RequestUri：{requestUri}");
                    }
                    else
                    {
                        logger.Error($"Request API => 請求失敗, 重試次數：{retryCount}, Exception：{result?.Exception?.Message}");
                    }
                });
        }
    }
}
