﻿using BloodHound.Domain.Dto;
using BloodHound.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Net;

namespace BloodHound.Infrastructure.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<HttpGlobalExceptionFilter> _logger;

        public HttpGlobalExceptionFilter(ILogger<HttpGlobalExceptionFilter> logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                context.Exception,
                context.Exception.Message);

            if (context.Exception.GetType() == typeof(DomainException) || context.Exception.GetType() == typeof(ValidatorException))
            {
                var json = new JsonErrorResponse
                {
                    Messages = context.Exception.Message
                };

                context.Result = new BadRequestObjectResult(json);
                if (context.Exception.GetType() == typeof(DomainException))
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.UnprocessableEntity;
                else
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
            else
            {
                var json = new JsonErrorResponse
                {
                    Messages = "系統發生錯誤，請稍後在試"
                };

                context.Result = new BadRequestObjectResult(json);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            context.ExceptionHandled = true;
        }
    }
}
