﻿using BloodHound.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BloodHound.Infrastructure.Data.Configurations;

public abstract class AuditableEntityConfigurationBase<T> : IEntityTypeConfiguration<T> where T : AuditableEntity
{
    public virtual void Configure(EntityTypeBuilder<T> builder)
    {
        builder.Property(e => e.CreateUserId)
            .HasColumnName("CreateUser")
            .IsRequired(false)
            .HasColumnType("uniqueidentifier")
            .HasComment("建立人員");
        builder.Property(e => e.CreateDate)
            .HasColumnName("CreateDate")
            .IsRequired(false)
            .HasComment("建立時間");
        builder.Property(e => e.CreateFromIp)
            .HasColumnName("CreateFrom")
            .IsRequired(false)
            .HasMaxLength(50)
            .HasComment("建立來源");
        builder.Property(e => e.ModifyUserId)
           .HasColumnName("ModifyUser")
           .IsRequired(false)
           .HasColumnType("uniqueidentifier")
           .HasComment("修改人員");
        builder.Property(e => e.ModifyDate)
            .HasColumnName("ModifyDate")
            .IsRequired(false)
            .HasComment("修改時間");
        builder.Property(e => e.ModifyFromIp)
            .HasColumnName("ModifyFrom")
            .IsRequired(false)
            .HasMaxLength(50)
            .HasComment("修改來源");
    }
}
